#include "pch.h"
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <set>
#include <QWidget>
#include <QtWidgets/QApplication>
#ifndef NDEBUG
	#include "../PokerChallenge/GeneratedFiles/Debug/moc_EquityCalculator.cpp"
#else
	#include "../PokerChallenge/GeneratedFiles/Release/moc_EquityCalculator.cpp"
#endif //NDEBUG
#include "../PokerChallenge/Constants.h"
#include "../PokerChallenge/Card.h"
#include "../PokerChallenge/Card.cpp"
#include "../PokerChallenge/Player.h"
#include "../PokerChallenge/Player.cpp"
#include "../PokerChallenge/Board.h"
#include "../PokerChallenge/Board.cpp"
#include "../PokerChallenge/HandValue.h"
#include "../PokerChallenge/HandValue.cpp"
#include "../PokerChallenge/HandValueCalculator.h"
#include "../PokerChallenge/HandValueCalculator.cpp"
#include "../PokerChallenge/EquityCalculator.h"
#include "../PokerChallenge/EquityCalculator.cpp"
#include "../PokerChallenge/StrategyCalculator.h"
#include "../PokerChallenge/StrategyCalculator.cpp"

using namespace poker;

using std::vector;
using std::ifstream;
using std::string;
using std::stringstream;
using std::unique_ptr;
using std::make_unique;
using std::pair;
using std::make_pair;
using std::set;
using std::out_of_range;

std::ostream& poker::operator<<(std::ostream& out, const Strategy& strategy);

TEST(CardTest, PositionInputTest) 
{
	Card c1(0), c2(14), c3(51);
	EXPECT_EQ(c1.GetDeckName(), "2h");
	EXPECT_EQ(c2.GetDeckName(), "3d");
	EXPECT_EQ(c3.GetDeckName(), "As");
}

TEST(CardTest, DeckNameInputTest1)
{		
	Card c("8s");
	EXPECT_EQ(c.GetDeckName(), "8s");
	EXPECT_EQ(c.GetPosition(), 45);
}

TEST(CardTest, DeathTest1)
{
	EXPECT_THROW(Card c("XX"), runtime_error);
}

TEST(CardTest, DeathTest2)
{
	EXPECT_THROW(Card c(""), runtime_error);
}

TEST(PlayerTest, PlayerCompareTest)
{
	Card p1c1(3), p1c2(17), p2c1(3), p2c2(17);
	Player p1, p2;
	vector<Card> p1_cards = { p1c1,p1c2 },
				 p2_cards = { p2c1,p2c2 };

	p1.SetCards(p1_cards);
	p2.SetCards(p2_cards);

	EXPECT_EQ(p1_cards, p2_cards);
}

TEST(BoardTest, ConsitencyDeathTest1)
{
	Board board;
	Card c1(3), c2(17), c3(3);
	vector<Card> flop = { c1,c2,c3 };
	board.SetFlop(flop);
	EXPECT_EQ(board.IsComplete(), false);
	Card c4(4);
	flop.push_back(c4);
	try
	{
		board.SetFlop(flop);
	}
	catch (const out_of_range& err)
	{
		EXPECT_EQ(err.what(), string("Board inconsistent"));
	}
}

TEST(BoardTest, ConsitencyDeathTest2)
{
	Board board;
	Card c1(34), c2(2);
	vector<Card> turn = { c1,c2 };
	
	try
	{
		board.SetTurn(turn);
	}
	catch (const out_of_range& err)
	{
		EXPECT_EQ(err.what(), string("Board inconsistent"));
	}
}

TEST(BoardTest, ConsitencyDeathTest3)
{
	Board board;
	Card c1(34), c2(2);
	vector<Card> river = { c1,c2 };

	try
	{
		board.SetRiver(river);
	}
	catch (const out_of_range& err)
	{
		EXPECT_EQ(err.what(), string("Board inconsistent"));
	}
}

TEST(BoardTest, CompleteTest)
{
	Board b;
	Card c1(1), c2(2), c3(14), c4(4), c5(5);

	b.SetFlop(vector<Card>{c1, c2, c3});
	EXPECT_EQ(b.IsComplete(), false);

	b.SetTurn(vector<Card>{c4});
	EXPECT_EQ(b.IsComplete(), false);

	b.SetRiver(vector<Card>{c5});
	EXPECT_EQ(b.IsComplete(), true);

}

int GetCard(const string& in)
{

	if (in.find("Deuces") != string::npos || in.find("Two") != string::npos)
		return 0;
	else if (in.find("Threes") != string::npos || in.find("Three") != string::npos)
		return 1;
	else if (in.find("Fours") != string::npos || in.find("Four") != string::npos)
		return 2;
	else if (in.find("Fives") != string::npos || in.find("Five") != string::npos)
		return 3;
	else if (in.find("Sixes") != string::npos || in.find("Six") != string::npos)
		return 4;
	else if (in.find("Sevens") != string::npos || in.find("Seven") != string::npos)
		return 5;
	else if (in.find("Eights") != string::npos || in.find("Eight") != string::npos)
		return 6;
	else if (in.find("Nines") != string::npos || in.find("Nine") != string::npos)
		return 7;
	else if (in.find("Tens") != string::npos || in.find("Ten") != string::npos)
		return 8;
	else if (in.find("Jacks") != string::npos || in.find("Jack") != string::npos)
		return 9;
	else if (in.find("Queens") != string::npos || in.find("Queen") != string::npos)
		return 10;
	else if (in.find("Kings") != string::npos || in.find("King") != string::npos)
		return 11;
	else if (in.find("Aces") != string::npos || in.find("Ace") != string::npos)
		return 12;
	else
		throw runtime_error("");


}

void GetKickers(const string& in, HandValue& hand_value)
{
	auto kicker_start = in.find("-"), kicker_separator = in.find("+");

	if (kicker_separator != string::npos)
	{
		string kicker_string = in.substr(kicker_start, string::npos);
		while (kicker_separator != string::npos)
		{
			hand_value.kickers.insert(GetCard(kicker_string.substr(0, kicker_separator - kicker_start)));
			kicker_string = kicker_string.substr(kicker_string.find("+") + 1, string::npos);
			kicker_separator = kicker_string.find("+");
		}
		hand_value.kickers.insert(GetCard(kicker_string));
	}
	else
		hand_value.kickers.insert(GetCard(in));
}

HandValue GetHandValueFromLine(const string& in)
{
	HandValue hand_value;
	hand_value.rank = UndefinedHandRank;

	auto pos = in.find("high card");
	if (pos != string::npos)
	{
		hand_value.rank = High_card;

		auto kicker_end = in.find("kicker");
		if (kicker_end != string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in, hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in));

		return hand_value;
	}

	pos = in.find("a pair of");
	if (pos != string::npos)
	{
		hand_value.rank = Pair;

		auto kicker_end = in.find("kicker");
		if (kicker_end != string::npos && in.find("lower kicker") == string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in.substr(kicker_start, string::npos), hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in));

		return hand_value;
	}

	pos = in.find("two pair");
	if (pos != string::npos)
	{
		hand_value.rank = Two_pairs;
		auto and = in.find("and");
		hand_value.heights.insert(GetCard(in.substr(pos, and-pos)));
		auto kicker_end = in.find("kicker");
		if (kicker_end != string::npos && in.find("lower kicker") == string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in.substr(kicker_start, string::npos), hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in));


		return hand_value;
	}

	pos = in.find("three of a kind");
	if (pos != string::npos)
	{
		hand_value.rank = Three_of_a_kind;
		auto kicker_end = in.find("kicker");
		if (kicker_end != string::npos && in.find("lower kicker") == string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in.substr(kicker_start, string::npos), hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in));

		return hand_value;
	}

	pos = in.find("a straight flush,");
	if (pos != string::npos)
	{
		hand_value.rank = Straight_flush;
		pos = in.find("to");
		hand_value.heights.insert(GetCard(in.substr(pos, string::npos)));
		return hand_value;
	}

	pos = in.find("a straight,");
	if (pos != string::npos)
	{
		hand_value.rank = Straight;
		pos = in.find("to");
		hand_value.heights.insert(GetCard(in.substr(pos, string::npos)));
		return hand_value;
	}

	pos = in.find("a flush,");
	if (pos != string::npos)
	{
		hand_value.rank = Flush;
		auto kicker_end = in.find("higher");
		if (kicker_end != string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in.substr(kicker_start, string::npos), hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in.substr(pos, string::npos)));

		return hand_value;
	}

	pos = in.find("a full house");
	if (pos != string::npos)
	{
		hand_value.rank = Full_house;
		auto to = in.find("full of");
		hand_value.heights.insert(GetCard(in.substr(pos, to - pos)));
		hand_value.kickers.insert(GetCard(in.substr(to, string::npos)));
		return hand_value;
	}

	pos = in.find("four of a kind");
	if (pos != string::npos)
	{
		hand_value.rank = Four_of_a_kind;

		auto kicker_end = in.find("kicker");
		if (kicker_end != string::npos && in.find("lower kicker") == string::npos)
		{
			auto kicker_start = in.find("-");
			hand_value.heights.insert(GetCard(in.substr(pos, kicker_start - pos)));
			GetKickers(in.substr(kicker_start, string::npos), hand_value);
		}
		else
			hand_value.heights.insert(GetCard(in));

		return hand_value;
	}

	pos = in.find("a Royal Flush");
	if (pos != string::npos)
	{
		hand_value.rank = Royal_flush;
		return hand_value;
	}

	return hand_value;
}

TEST(HandValueTest, CompareTest1)
{
	HandValue h1, h2;

	h1.rank = Three_of_a_kind;
	h2.rank = Two_pairs;

	h1.heights = set<int>{ 1,2,3,4 };
	h2.heights = set<int>{ 1,2,3,4 };

	EXPECT_LT(h2, h1);
	EXPECT_LE(h2, h1);

	h2.rank = Three_of_a_kind;

	h2.heights.insert(5);

	//h1 heights = 1,2,3,4
	//h2 heights = 1,2,3,4,5

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.heights.insert(5);
	h1.heights.erase(1);

	//h1 heights = 2,3,4,5
	//h2 heights = 1,2,3,4,5

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.heights.insert(1);

	//h1 heights = 1,2,3,4,5
	//h2 heights = 1,2,3,4,5

	EXPECT_EQ(h1, h2);
	EXPECT_LE(h1, h2);

	h2.kickers.insert(6);

	//h1 heights = 1,2,3,4,5
	//h2 heights = 1,2,3,4,5
	//h1 kickers = 
	//k2 kickers = 6

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.kickers.insert(3);

	//h1 heights = 1,2,3,4,5
	//h2 heights = 1,2,3,4,5
	//h1 kickers = 3
	//k2 kickers = 6

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.kickers.insert(5);

	//h1 heights = 1,2,3,4,5
	//h2 heights = 1,2,3,4,5
	//h1 kickers = 3, 5
	//k2 kickers = 6

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.kickers.insert(6);

	//h1 heights = 1,2,3,4,5
	//h2 heights = 1,2,3,4,5
	//h1 kickers = 3, 5, 6
	//k2 kickers = 6

	EXPECT_LT(h2, h1);
	EXPECT_LE(h2, h1);
}

TEST(HandValueTest, CompareTest2)
{
	HandValue h1, h2;

	h1.rank = High_card;
	h2.rank = Four_of_a_kind;

	h1.heights.insert(12);

	EXPECT_LT(h1, h2);
	EXPECT_LE(h1, h2);

	h1.rank = Straight_flush;

	EXPECT_LT(h2, h1);
	EXPECT_LE(h2, h1);

	h1.rank = High_card;
	h2.rank = UndefinedHandRank;

	EXPECT_LT(h2, h1);
	EXPECT_LE(h2, h1);
}

TEST(HandValueCalculatorTest, CompareTestWithDownloadedHH)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);
	ifstream handhistory("../PokerChallenge/handhistory.txt");
	
	if (!handhistory.is_open())
		handhistory.open("handhistory.txt");

	EXPECT_EQ(handhistory.is_open(), true);

	string line;
	bool test_ok = true;
	int game = 0;
	int test_case_count = std::count(std::istream_iterator<string>(handhistory), std::istream_iterator<string>(), "Board");
	handhistory.clear();
	handhistory.seekg(0, std::ios::beg);
	while (!handhistory.eof())
	{
		unique_ptr<Board> board;
		vector<Player> players;
		set<int> original_winners;
		stringstream original_ss, original_value;
		vector<pair<Player, HandValue>> original_players_with_handvalue;
		stringstream message_ss;
		while (getline(handhistory, line) && line != "")
		{
			if (line.find("Board") != string::npos)
			{
				game++;
				vector<Card> flop, turn, river;
				string board_line = line.substr(line.find("[") + 1, 14);

				size_t i = 0;
				for (; i < board_line.size() && i < 9; i += 3)
					flop.push_back(Card(board_line.substr(i, 2)));

				turn.push_back(Card(board_line.substr(i, 2)));
				i += 3;
				river.push_back(Card(board_line.substr(i, 2)));
				board = make_unique<Board>(flop, turn, river);

			}
			else
			{
				string player_line = line.substr(line.find("[") + 1, 5);

				Card first_card(player_line.substr(0, 2)),
					second_card(player_line.substr(3, 2));

				int id = line[0] - '0';
				vector<Card> player_cards = { first_card ,second_card };
				Player player(id, player_cards, UndefinedPosition);
				players.push_back(player);

				if (line.find("won") != string::npos)
					original_winners.insert(id);

				original_players_with_handvalue.push_back(make_pair(player, GetHandValueFromLine(line)));
			}
			original_ss << line << "\n";
		}

		EquityCalculator eq(players, *board, 10);
		set<int> calculated_winners;
		vector<pair<Player, HandValue>> players_with_handvalue = eq.EvaluatePlayers(calculated_winners);

		bool equal = true;
		if (original_winners.size() == calculated_winners.size() && original_winners != calculated_winners)
			equal = false;
		else
		{
			for (size_t i = 0; i < players_with_handvalue.size(); i++)
			{
				if (players_with_handvalue[i].second.rank != original_players_with_handvalue[i].second.rank ||
					(players_with_handvalue[i].second.rank == original_players_with_handvalue[i].second.rank && players_with_handvalue[i].second.heights != original_players_with_handvalue[i].second.heights))
					equal = false;
			}
		}

		if (!equal)
		{
			test_ok = false;
			break;
		}
	}
	
	EXPECT_EQ(test_ok, true);
}

TEST(StrategyCalculatorTest, PreFlopTest1)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card A1("Ah"), A2("Ac");
	Board b;
	Player hero(5, vector<Card>{A1, A2}, MP3);
	
	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);


	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	ASSERT_NEAR(hero.GetEquity(), 30.0, 2);

	StrategyCalculator sc(b, unordered_set<PlayerType>{});
	sc.CalculateStrategy(hero, ReRaise, 9);
	EXPECT_EQ(hero.GetPreflopStrategy(), ReRaise);
}

TEST(StrategyCalculatorTest, PreFlopTest2)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card A1("8h"), A2("8c");
	Board b;
	Player hero(5, vector<Card>{A1, A2}, MP3);

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());
	
	StrategyCalculator sc(b, unordered_set<PlayerType>{});

	sc.CalculateStrategy(hero, ReRaise, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Fold);

	sc.CalculateStrategy(hero, Call, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Call);

	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Call);

	hero.SetPosition(CO);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), RaiseFold);

	hero.SetPosition(SB);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Call);

	hero.SetPosition(UTG);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Fold);
}

TEST(StrategyCalculatorTest, PreFlopTest3)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card A1("Ah"), A2("Jc");
	Board b;
	Player hero(5, vector<Card>{A1, A2}, MP3);

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	StrategyCalculator sc(b, unordered_set<PlayerType>{});

	sc.CalculateStrategy(hero, Raise, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Fold);

	sc.CalculateStrategy(hero, Call, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Fold);

	hero.SetPosition(CO);
	sc.CalculateStrategy(hero, Call, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), RaiseFold);

	hero.SetPosition(MP2);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), RaiseFold);

	hero.SetPosition(CO);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), RaiseFold);

	hero.SetPosition(SB);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), RaiseFold);

	hero.SetPosition(UTG);
	sc.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetPreflopStrategy(), Fold);
}

TEST(StrategyCalculatorTest, FlopMonsterHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Kc"), a2("Jc");
	Card f1("Qc"), f2("Tc"), f3("9c");
	vector<Card> flop = {f1,f2,f3}, turn, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, SB);

	hero.SetPreflopStrategy(RaiseFold);

	StrategyCalculator sc(b, unordered_set<PlayerType>{});
	sc.CalculateStrategy(hero, CheckFold, 9);
	EXPECT_EQ(hero.GetHandValue().rank, Straight_flush);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);

	StrategyCalculator sc2(b, unordered_set<PlayerType>{Rock, Maniac});
	sc2.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetFlopStrategy(), SlowPlay);

	sc2.CalculateStrategy(hero, Raise, 9);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);
}

TEST(StrategyCalculatorTest, FlopCompleteHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Ac"), a2("Jc");
	Card f1("Qc"), f2("Tc"), f3("9c");
	vector<Card> flop = { f1,f2,f3 }, turn, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, UTG1);

	hero.SetPreflopStrategy(RaiseFold);

	StrategyCalculator sc(b, unordered_set<PlayerType>{});
	sc.CalculateStrategy(hero, CheckFold, 9);
	EXPECT_EQ(hero.GetHandValue().rank, Flush);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);

	StrategyCalculator sc2(b, unordered_set<PlayerType>{Rock, Maniac});
	sc2.CalculateStrategy(hero, CheckFold, 9);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);

	sc2.CalculateStrategy(hero, ReRaise, 9);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);

	Card b1("9d"), b2("9s");
	hero.SetCards(vector<Card>{b1, b2});

	sc.CalculateStrategy(hero, CheckFold, 9);
	EXPECT_EQ(hero.GetHandValue().rank, Three_of_a_kind);
	ASSERT_EQ(hero.GetFlopStrategy(), ReRaise);
}

TEST(StrategyCalculatorTest, FlopStrongDrawHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Ac"), a2("Jc");
	Card f1("2h"), f2("Tc"), f3("9c");
	vector<Card> flop = { f1,f2,f3 }, turn, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, UTG1);
	int opponent_count = 3;

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc(b, unordered_set<PlayerType>{});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, High_card);
	ASSERT_EQ(hero.GetFlopStrategy(), RaiseCall);

	sc.CalculateStrategy(hero, ReRaise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), CheckFold);

	opponent_count = 1;
	sc.CalculateStrategy(hero, ReRaise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), RaiseFold);

	hero.SetPreflopStrategy(Call); //opponent has preflop initiative

	sc.CalculateStrategy(hero, Raise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), Call);

	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), Call);
}

TEST(StrategyCalculatorTest, FlopCBetTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Ac"), a2("Td");
	Card f1("2h"), f2("Tc"), f3("9c");
	vector<Card> flop = { f1,f2,f3 }, turn, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, UTG1);
	int opponent_count = 1;

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc(b, unordered_set<PlayerType>{Rock});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Pair);
	ASSERT_EQ(hero.GetFlopStrategy(), RaiseFold);

	sc.CalculateStrategy(hero, Raise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), CheckFold);
}

TEST(StrategyCalculatorTest, FlopStrongHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Ac"), a2("Tc");
	Card f1("2c"), f2("Th"), f3("9c");
	vector<Card> flop = { f1,f2,f3 }, turn, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, UTG1);
	int opponent_count = 2;

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc(b, unordered_set<PlayerType>{Rock});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Pair);
	ASSERT_EQ(hero.GetFlopStrategy(), RaiseCall);

	sc.CalculateStrategy(hero, Raise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), RaiseFold);

	hero.SetPreflopStrategy(Call); //opponent has preflop initiative
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), Call);

	sc.CalculateStrategy(hero, Raise, opponent_count);
	ASSERT_EQ(hero.GetFlopStrategy(), Call);
}

TEST(StrategyCalculatorTest, TurnMonsterHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("5d"), a2("5c");
	Card f1("5s"), f2("Th"), f3("9c");
	Card t("9s");
	vector<Card> flop = { f1,f2,f3 }, turn = { t }, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, MP3);
	int opponent_count = 1;

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc(b, unordered_set<PlayerType>{Rock});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Full_house);
	ASSERT_EQ(hero.GetTurnStrategy(), ReRaise);

	StrategyCalculator sc2(b, unordered_set<PlayerType>{LAG});
	sc2.CalculateStrategy(hero, CheckFold, opponent_count);
	ASSERT_EQ(hero.GetTurnStrategy(), SlowPlay);
}

TEST(StrategyCalculatorTest, TurnCompleteHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Jd"), a2("8c");
	Card f1("6s"), f2("Th"), f3("9c");
	Card t("7s");
	vector<Card> flop = { f1,f2,f3 }, turn = { t }, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, MP3);
	int opponent_count = 1;

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc2(b, unordered_set<PlayerType>{LAG});
	sc2.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Straight);
	ASSERT_EQ(hero.GetTurnStrategy(), ReRaise);
}

TEST(StrategyCalculatorTest, TurnMiddleHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Jd"), a2("8c");
	Card f1("As"), f2("Jh"), f3("Kc");
	Card t("8d");
	vector<Card> flop = { f1,f2,f3 }, turn = { t }, river;
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, MP3);
	int opponent_count = 2;

	EquityCalculator ec(vector<Player>{hero}, b, 10);
	ec.SimulateEquityWithMonteCarlo(1000);

	for (const auto& player : ec.GetKnownPlayers())
		if (5 == player.GetID())
			hero.SetEquity(player.GetEquity());

	hero.SetPreflopStrategy(RaiseFold); //hero has preflop initiative

	StrategyCalculator sc2(b, unordered_set<PlayerType>{LAG});
	sc2.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Two_pairs);
	ASSERT_EQ(hero.GetTurnStrategy(), RaiseFold);
}

TEST(StrategyCalculatorTest, RiverCompleteHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Jd"), a2("8c");
	Card f1("As"), f2("Qh"), f3("Kc");
	Card t("8d");
	Card r("8h");
	vector<Card> flop = { f1,f2,f3 }, turn = { t }, river = { r };
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, MP3);
	int opponent_count = 1;

	StrategyCalculator sc(b, unordered_set<PlayerType>{LAG});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Three_of_a_kind);
	ASSERT_EQ(hero.GetRiverStrategy(), ReRaise);

	sc.CalculateStrategy(hero, Raise, opponent_count);
	ASSERT_EQ(hero.GetRiverStrategy(), ReRaise);
}

TEST(StrategyCalculatorTest, RiverMonsterHandTest)
{
	int argc = 0;
	char* argv;
	QApplication a(argc, &argv);

	Card a1("Ad"), a2("Kd");
	Card f1("Qs"), f2("Qd"), f3("Kc");
	Card t("Jd");
	Card r("Td");
	vector<Card> flop = { f1,f2,f3 }, turn = { t }, river = { r };
	Board b(flop, turn, river);
	Player hero(5, vector<Card>{a1, a2}, MP3);
	int opponent_count = 1;

	StrategyCalculator sc(b, unordered_set<PlayerType>{});
	sc.CalculateStrategy(hero, CheckFold, opponent_count);
	EXPECT_EQ(hero.GetHandValue().rank, Royal_flush);
	ASSERT_EQ(hero.GetRiverStrategy(), ReRaise);
}