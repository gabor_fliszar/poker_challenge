#include "HandValue.h"

using std::set;
namespace poker
{
	bool set_greather(const set<int> &s1, const set<int> &s2)
	{
		auto it1 = s1.rbegin(), it2 = s2.rbegin();
		while (it1 != s1.rend() && it2 != s2.rend() && *it1 == *it2)
		{
			++it1;
			++it2;
		}

		if (it1 == s1.rend() && it2 == s2.rend())
			return false;
		else if (it1 == s1.rend())
			return true;
		else if (it2 == s2.rend())
			return false;
		else
			return *it1 < *it2;
	}

	bool HandValue::operator<(const HandValue & rhs) const
	{
		if (rank < rhs.rank)
			return true;
		else if (rank == rhs.rank)
		{
			if (heights != rhs.heights)
				return set_greather(heights, rhs.heights);
			else
				return set_greather(kickers, rhs.kickers);
		}
		
		return false;
	}

	bool HandValue::operator==(const HandValue & rhs) const
	{
		return rank == rhs.rank && heights == rhs.heights && kickers == rhs.kickers;
	}

	bool HandValue::operator<=(const HandValue & rhs) const
	{
		return *this == rhs || *this < rhs;
	}

	std::ostream& operator<<(std::ostream& out, const HandRank& value)
	{
		switch (value)
		{
			case High_card:out << "High card"; break;
			case Pair:out << "Pair"; break;
			case Two_pairs:out << "Two pairs"; break;
			case Three_of_a_kind:out << "Three of a kind"; break;
			case Straight:out << "Straight"; break;
			case Flush:out << "Flush"; break;
			case Full_house:out << "Full house"; break;
			case Four_of_a_kind:out << "Four of a kind"; break;
			case Straight_flush:out << "Straight flush"; break;
			case Royal_flush:out << "Royal flush"; break;
		}

		return out;
	}
}