#pragma once
#include "Constants.h"
#include <ostream>
#include <set>

namespace poker
{
	//Struct for storing the hand value of a player. It has 3 member: 
	//rank - The rank of player hand
	//heights - The heights of rank
	//kicker - It counts when 2 or more player has a same hand rank and heigts
	struct HandValue
	{
		HandRank rank;
		std::set<int> heights;
		std::set<int> kickers;

		HandValue() :rank(UndefinedHandRank) {}
		HandValue(HandRank rank, const std::set<int>& heights, const std::set<int>& kickers) : 
			rank(rank), heights(heights), kickers(kickers) {}

		bool operator <(const HandValue & rhs) const;
		bool operator ==(const HandValue & rhs) const;
		bool operator <=(const HandValue & rhs) const;
	};

	std::ostream& operator<<(std::ostream& out, const HandRank& value);
}