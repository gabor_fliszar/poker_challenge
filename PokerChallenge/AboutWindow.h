#pragma once

#include <QWidget>
#include <QDialog>
#include "ui_AboutWindow.h"

namespace poker
{
	class AboutWindow : public QDialog
	{
		Q_OBJECT

	public:
		AboutWindow(QWidget *parent = Q_NULLPTR);

	private:
		Ui::AboutWindow ui;
	};
}