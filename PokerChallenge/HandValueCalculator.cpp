#include "HandValueCalculator.h"
#include <set>
#include <algorithm>

using std::map;
using std::unordered_set;
using std::vector;
using std::set;
using std::max;

namespace poker
{
	HandValueCalculator::HandValueCalculator(const Board & board) :
		m_board(board), m_card_suits(4, 0)
	{
	}

	HandValue HandValueCalculator::CalculateHandValue(const Player & player)
	{
		HandValue hand_value;
		InitParameters(player);
		PreCheckPossibleStraightAndFlush();

		CheckStraightAndFlush(hand_value);

		if (hand_value.rank == Royal_flush || hand_value.rank == Straight_flush)
			return hand_value;
		else
			CheckRepeatingTypeRanks(hand_value);

		return hand_value;
	}

	void HandValueCalculator::InitParameters(const Player & player)
	{
		m_allcards.clear();

		for (const auto& board_card : m_board.GetFlop())
			m_allcards[board_card.GetPosition() % kPlayingCardTypeCount].insert(board_card.GetPosition() / kPlayingCardTypeCount);

		for (const auto& board_card : m_board.GetTurn())
			m_allcards[board_card.GetPosition() % kPlayingCardTypeCount].insert(board_card.GetPosition() / kPlayingCardTypeCount);

		for (const auto& board_card : m_board.GetRiver())
			m_allcards[board_card.GetPosition() % kPlayingCardTypeCount].insert(board_card.GetPosition() / kPlayingCardTypeCount);

		for (const auto& player_card : player.GetCards())
			m_allcards[player_card.GetPosition() % kPlayingCardTypeCount].insert(player_card.GetPosition() / kPlayingCardTypeCount);

		m_height = 0;
		m_kicker = 0;
		m_rank = High_card;

		m_first_card_type = player.GetCards()[0].GetPosition() % kPlayingCardTypeCount;
		m_second_card_type = player.GetCards()[1].GetPosition() % kPlayingCardTypeCount;
		m_longest_straight = 0;
		m_highest_straight_card = -1;
		m_staright_flush = false;
	}

	void HandValueCalculator::PreCheckPossibleStraightAndFlush()
	{
		int next = -1;
		int index = 6;

		//It iterates reverse over m_allcards and looks for sequence to determine Straight. 
		for (auto it = m_allcards.rbegin(); it != m_allcards.rend(); ++it)
		{
			int card = it->first;

			if (card != next && index > 2)
			{
				m_possible_staright_flush_types.clear();
				m_possible_staright_flush_types.insert(it->second.begin(), it->second.end());
				m_highest_straight_card = card;
				next = card - 1;
				m_longest_straight = 1;
				m_staright_flush = true;

			}
			else if (card == next)
			{
				unordered_set<int> intersection;

				//It checks the possible Straight_flush also.
				for (const auto& possible_staright_flush_type : m_possible_staright_flush_types)
					if (it->second.find(possible_staright_flush_type) != it->second.end())
						intersection.insert(possible_staright_flush_type);

				if (intersection.empty())
					m_staright_flush = false;
				else
					m_possible_staright_flush_types = intersection;

				m_longest_straight++;
				next--;
			}

			//fill m_card_suits to determine Flush
			for (const auto& suit : it->second)
				m_card_suits[suit]++;

			//fill m_card_types to determine repeating type of ranks (pair, two pair etc...)
			m_card_types[card] = static_cast<int>(it->second.size());

			index--;
		}

		// special case to check of A,2,3,4,5 type straight
		if (((m_longest_straight == 4 && m_highest_straight_card == 3) ||
			(m_longest_straight == 5 && m_highest_straight_card == 4) ||
			(m_longest_straight == 6 && m_highest_straight_card == 6))
			&& m_allcards.rbegin()->first == kAcePosition) 
		{
			m_longest_straight++;

			// check for A,2,3,4,5 type straight flush
			if (m_staright_flush)
			{
				bool found = false;
				for (const auto& s : m_possible_staright_flush_types)
				{
					//m_allcards.rbegin() is in this case A, and it searches possible flush suit of A 
					if (m_allcards.rbegin()->second.find(s) != m_allcards.rbegin()->second.end())
					{
						found = true;
						break;
					}
				}
				if (!found)
					m_staright_flush = false;
			}
		}
	}

	void HandValueCalculator::CheckStraightAndFlush(HandValue& hand_value)
	{
		if (m_longest_straight > 4)
		{
			if (m_height == 0)
				m_kicker = 0;

			//if we have 6 or 7 length of Straight, check possible Straight flush 
			if (m_longest_straight > 5)
			{
				m_staright_flush = true;
				int staright_flush_height = -1;

				//iterate reverse over m_allcards
				for (auto it = m_allcards.rbegin(); it != m_allcards.rend() && m_staright_flush; ++it)
				{
					//found the highet Straight card position in m_allcards
					if (it->first == m_highest_straight_card)
					{
						m_possible_staright_flush_types.clear();
						m_possible_staright_flush_types.insert(it->second.begin(), it->second.end());
						staright_flush_height = it->first;
						//move to the next element
						++it;
						int straight_length = m_longest_straight - 1;

						//looking for Straight flush
						while (it != m_allcards.rend() && m_staright_flush)
						{
							unordered_set<int> intersection;

							for (const auto& possible_staright_flush_type : m_possible_staright_flush_types)
								if (it->second.find(possible_staright_flush_type) != it->second.end())
									intersection.insert(possible_staright_flush_type);

							//not found possible Straight flush in this position
							if (intersection.empty())
							{
								//if the Straight length > 4, this Straight flush is possible so
								//check again the search for Straight flush from this position
								if (straight_length > 4)
								{
									m_possible_staright_flush_types.clear();
									m_possible_staright_flush_types.insert(it->second.begin(), it->second.end());
									staright_flush_height = it->first;
								}
								//special case to determine A, 2, 3, 4, 5 type Straight flush
								else if (straight_length == 4 && m_allcards.rbegin()->first == kAcePosition)
								{
									m_possible_staright_flush_types.clear();
									m_possible_staright_flush_types.insert(it->second.begin(), it->second.end());
									staright_flush_height = it->first;

									bool found = false;

									for (const auto& possible_staright_flush_type : m_possible_staright_flush_types)
										if (m_allcards.rbegin()->second.find(possible_staright_flush_type) != m_allcards.rbegin()->second.end())
										{
											found = true;
											break;
										}

									if (!found)
									{
										m_staright_flush = false;
										break;
									}
								}
								else
								{
									m_staright_flush = false;
									break;
								}
							}
							else
								m_possible_staright_flush_types = intersection;

							straight_length--;
							++it;
						}
						break;
					}
				}
				if (m_staright_flush)
					m_highest_straight_card = staright_flush_height;
				
			}

			if (m_staright_flush)
			{
				if (m_highest_straight_card == kAcePosition)
				{
					m_rank = Royal_flush;
					hand_value.rank = m_rank;
					hand_value.heights = set<int>();
					hand_value.kickers = set<int>();
					return;
				}
				else
				{
					m_rank = Straight_flush;

					hand_value.rank = m_rank;
					hand_value.heights = { m_highest_straight_card };
					hand_value.kickers = set<int>();
					return;
				}
			}
			else
			{
				m_rank = Straight;
				hand_value.rank = m_rank;
				hand_value.heights = { m_highest_straight_card };
			}
		}
		else
			m_height = 0;

		//looking for Flush
		for (size_t i = 0; i < m_card_suits.size(); i++)
		{
			if (m_card_suits[i] > 4)
			{
				m_rank = Flush;
				m_height = -1;
				set<int> kickers;
				for (auto it = m_allcards.rbegin(); it != m_allcards.rend(); ++it)
				{
					if (it->second.find(i) != it->second.end())
					{
						if (m_height == -1)
							m_height = it->first;
						else
							kickers.insert(it->first);

						if (m_height != -1 && kickers.size() == 4)
							break;
					}
				}
				hand_value.rank = m_rank;
				hand_value.heights = { m_height };
				hand_value.kickers = kickers;
				break;
			}
		}
	}

	void HandValueCalculator::CheckRepeatingTypeRanks(HandValue & hand_value)
	{
		for (const auto& card : m_card_types)
		{
			if (card.second == 4)
			{
				m_rank = Four_of_a_kind;
				m_kicker = -1;
				if (m_first_card_type != card.first && m_second_card_type != card.first)
				{
					m_height = card.first;
					m_kicker = max(m_first_card_type, m_second_card_type);
				}
				else if (m_first_card_type != card.first)
				{
					m_height = m_second_card_type;
					m_kicker = m_first_card_type;
				}
				else if (m_second_card_type != card.first)
				{
					m_height = m_first_card_type;
					m_kicker = m_second_card_type;
				}
				else
					m_height = m_first_card_type;

				set<int> heights = { m_height };
				set<int> kickers;

				if (m_kicker != -1)
					kickers.insert(m_kicker);

				hand_value.rank = m_rank;
				hand_value.heights = heights;
				hand_value.kickers = kickers;
				return;
			}
			else if (card.second == 3)
			{
				if (m_rank == Pair || m_rank == Two_pairs || m_rank == Three_of_a_kind)
				{
					if (m_rank == Three_of_a_kind)
					{
						if (*hand_value.heights.rbegin() < card.first)
						{
							hand_value.kickers = hand_value.heights;
							hand_value.heights.clear();
							hand_value.heights = { card.first };
						}
						else
							hand_value.kickers = { card.first };
					}
					else
					{
						hand_value.kickers = { *hand_value.heights.rbegin() };
						hand_value.heights = { card.first };
					}
					m_rank = Full_house;
					hand_value.rank = m_rank;
				}
				else
				{
					if (m_rank != Flush && m_rank != Straight)
					{
						m_rank = Three_of_a_kind;
						set<int> kickers, heights = { card.first };
						m_height = card.first;

						for (auto it = m_allcards.rbegin(); it != m_allcards.rend() && kickers.size() != 2; ++it)
							if (it->first != card.first)
								kickers.insert(it->first);

						hand_value.rank = m_rank;
						hand_value.heights = heights;
						hand_value.kickers = kickers;
					}
				}
			}
			else if (card.second == 2)
			{
				if (m_rank != Straight &&
					m_rank != Flush)
				{
					if (m_rank == Three_of_a_kind || m_rank == Full_house)
					{
						if (m_rank == Full_house && *hand_value.kickers.rbegin() < card.first)
							hand_value.kickers.clear();

						m_rank = Full_house;

						hand_value.rank = m_rank;
						hand_value.kickers = { card.first };
					}
					else if (m_rank == Pair || m_rank == Two_pairs)
					{


						if (m_rank == Two_pairs)
						{
							m_rank = Two_pairs;
							hand_value.rank = m_rank;
							int swap_element = -1;
							for (const auto& h : hand_value.heights)
								if (h < card.first)
								{
									swap_element = h;
									break;
								}
							if (swap_element > -1)
							{
								hand_value.heights.erase(swap_element);
								hand_value.heights.insert(card.first);
							}
						}
						else
						{
							m_rank = Two_pairs;
							hand_value.rank = m_rank;
							hand_value.heights.insert(card.first);
						}

						set<int> kickers;

						for (auto it = m_allcards.rbegin(); it != m_allcards.rend() && kickers.empty(); ++it)
							if (hand_value.heights.find(it->first) == hand_value.heights.end())
								kickers.insert(it->first);

						hand_value.kickers = kickers;

					}
					else
					{

						m_rank = Pair;
						hand_value.rank = m_rank;
						hand_value.heights = { card.first };
						m_height = card.first;

						set<int> kickers;

						for (auto it = m_allcards.rbegin(); it != m_allcards.rend() && kickers.size() != 3; ++it)
							if (hand_value.heights.find(it->first) == hand_value.heights.end())
								kickers.insert(it->first);

						hand_value.kickers = kickers;

					}
				}
			}
			else
			{
				if (m_rank != Full_house &&
					m_rank != Three_of_a_kind &&
					m_rank != Straight &&
					m_rank != Flush &&
					m_rank != Two_pairs &&
					m_rank != Pair)
				{
					m_rank = High_card;
					hand_value.rank = m_rank;
					hand_value.heights = { card.first };

					set<int> kickers;

					for (auto it = m_allcards.rbegin(); it != m_allcards.rend() && kickers.size() != 4; ++it)
						if (hand_value.heights.find(it->first) == hand_value.heights.end())
							kickers.insert(it->first);

					hand_value.kickers = kickers;

				}
			}
		}
	}
}
