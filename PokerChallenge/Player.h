#pragma once
#include "Card.h"
#include "Constants.h"
#include "HandValue.h"
#include <vector>

namespace poker
{
	//This class represents a poker player at the table
	class Player
	{
		int m_id;
		std::vector<Card> m_cards;
		Position m_position;
		double m_equity;
		Strategy m_preflop_strategy;
		Strategy m_flop_strategy;
		Strategy m_turn_strategy;
		Strategy m_river_strategy;
		HandValue m_hand_value;
		PlayerType m_type;

	public:
		Player(int id, const std::vector<Card>& player_cards, Position position);
		Player() = default;
		
		void SetCards(const std::vector<Card>& player_cards);
		void SetEquity(double equity);
		void SetPosition(Position position);
		void SetPreflopStrategy(Strategy preflop_strategy = UndefinedStrategy);
		void SetFlopStrategy(Strategy flop_startegy = UndefinedStrategy);
		void SetTurnStrategy(Strategy turn_strategy = UndefinedStrategy);
		void SetRiverStrategy(Strategy river_strategy = UndefinedStrategy);
		void SetHandValue(const HandValue& hand_value);
		void SetType(PlayerType type);

		Strategy GetPreflopStrategy() const;
		Strategy GetFlopStrategy() const;
		Strategy GetTurnStrategy() const;
		Strategy GetRiverStrategy() const;
		PlayerType GetPlayerType() const;
		const HandValue& GetHandValue() const;
		const std::vector<Card>& GetCards() const;		
		double GetEquity() const;
		Position GetPosition() const;
		int GetID() const;

		void ClearPlayerCards();

		bool operator ==(const Player & rhs) const;
		bool operator !=(const Player & rhs) const;
	};
}