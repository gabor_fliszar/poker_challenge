#pragma once
#include "Card.h"
#include "Constants.h"
#include <vector>

namespace poker
{
	//Class for storing board cards(flop, turn and river)
	class Board
	{
		std::vector<Card> m_flop;
		std::vector<Card> m_turn;
		std::vector<Card> m_river;

		bool is_complete;
		void Check();

	public:
		Board();
		Board(const std::vector<Card>& flop, const std::vector<Card>& turn, const std::vector<Card>& river);

		const std::vector<Card>& GetFlop() const;
		const std::vector<Card>& GetTurn() const;
		const std::vector<Card>& GetRiver() const;

		void SetFlop(const std::vector<Card>& flop);
		void SetTurn(const std::vector<Card>& turn);
		void SetRiver(const std::vector<Card>& river);

		void ClearBoard(bool flop_set = false, bool turn_set = false, bool river_set = false);
		bool IsComplete() const;
	};
}