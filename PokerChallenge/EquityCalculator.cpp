#include "EquityCalculator.h"
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <set>
#include <map>
#include <sstream>
#include <iomanip> 
#include "HandValueCalculator.h"

using std::vector;
using std::string;
using std::stringstream;
using std::endl;
using std::unordered_set;
using std::pair;
using std::make_pair;
using std::set;
using std::map;
using std::max;
using std::setprecision;
using std::to_string;
using std::runtime_error;

namespace poker
{
	void EquityCalculator::GenerateCardsForPlayers()
	{

		for (size_t i = 0; i < m_unknown_players.size(); i++)
		{
			vector<Card> cards = GenerateRandomCards(kPlayerCardsCount);

			m_unknown_players[i].ClearPlayerCards();
			m_unknown_players[i].SetCards(cards);
		}
	}

	void EquityCalculator::GenerateBoard()
	{
		if (!m_flop_set)
		{
			vector<Card> flop_cards = GenerateRandomCards(kFlopCount);
			m_board.SetFlop(flop_cards);
		}

		if (!m_turn_set)
		{
			vector<Card> turn_card = GenerateRandomCards(kTurnCount);
			m_board.SetTurn(turn_card);
		}

		if (!m_river_set)
		{
			vector<Card> river_card = GenerateRandomCards(kRiverCount);
			m_board.SetRiver(river_card);
		}
	}

	vector<Card> EquityCalculator::GenerateRandomCards(int count)
	{
		vector<Card> cards;
		auto random = m_unselected_cards.begin();

		for (int i = 0; i < count; i++)
		{
			random = m_unselected_cards.begin();
			advance(random, rand() % m_unselected_cards.size());

			Card card(*random);
			m_unselected_cards.erase(card);
			m_selected_cards.insert(card);

			cards.push_back(card);
		}

		return cards;
	}

	EquityCalculator::EquityCalculator(const std::vector<Player>& players, const Board & board, int max_player) :
		m_board(board), m_max_player(max_player)
	{
		for (int i = 0; i < kPlayingCardsCount; i++)
			m_unselected_cards.insert(i);

		unordered_set<int> known_ids;
		for (const auto& p : players)
		{
			if (!p.GetCards().empty())
			{
				known_ids.insert(p.GetID());
				m_known_players.push_back(p);
				for (const auto& c : p.GetCards())
				{
					m_selected_cards.insert(c);
					m_unselected_cards.erase(c);
				}
			}
		}


		if (board.GetFlop().empty())
			m_flop_set = false;
		else
		{
			m_flop_set = true;
			for (const auto& flop_card : board.GetFlop())
			{
				m_selected_cards.insert(flop_card);
				m_unselected_cards.erase(flop_card);
			}
		}

		if (board.GetTurn().empty())
			m_turn_set = false;
		else
		{
			m_turn_set = true;
			for (const auto& turn_card : board.GetTurn())
			{
				m_selected_cards.insert(turn_card);
				m_unselected_cards.erase(turn_card);
			}
		}

		if (board.GetRiver().empty())
			m_river_set = false;
		else
		{
			m_river_set = true;
			for (const auto& river_card : board.GetRiver())
			{
				m_selected_cards.insert(river_card);
				m_unselected_cards.erase(river_card);
			}
		}

		for (int i = 0; i < max_player; i++)
			if (known_ids.find(i) == known_ids.end())
				m_unknown_players.push_back(Player(i, vector<Card>(), UndefinedPosition));
	}

	vector<pair<Player, HandValue>> EquityCalculator::EvaluatePlayers(set<int>& winners) const
	{
		vector<pair<Player, HandValue>> players_with_handvalue;
		HandValue max_hand_value;
		
		for (size_t i = 0; i < m_known_players.size(); i++)
		{
			HandValueCalculator hv_calculator(m_board);
			HandValue player_hand_value = hv_calculator.CalculateHandValue(m_known_players[i]);
			players_with_handvalue.push_back(make_pair(m_known_players[i], player_hand_value));
			if (max_hand_value <= player_hand_value)
			{
				if (max_hand_value < player_hand_value)
					winners.clear();

				winners.insert(m_known_players[i].GetID());
				max_hand_value = player_hand_value;
			}
		}
		for (size_t i = 0; i < m_unknown_players.size(); i++)
		{
			if (!m_unknown_players[i].GetCards().empty())
			{
				HandValueCalculator hv_calculator(m_board);
				HandValue player_hand_value = hv_calculator.CalculateHandValue(m_unknown_players[i]);
				
				players_with_handvalue.push_back(make_pair(m_unknown_players[i], player_hand_value));
				
				if (max_hand_value <= player_hand_value)
				{
					if (max_hand_value < player_hand_value)
						winners.clear();

					winners.insert(m_unknown_players[i].GetID());
					max_hand_value = player_hand_value;
				}
			}
		}
		return players_with_handvalue;
	}

	void EquityCalculator::SimulateEquityWithMonteCarlo(int test_cases)
	{
		vector<pair<Player, HandValue>> players_with_handvalue;
		set<int> calculated_winners;
		vector<double> player_wins(10, 0.0);
		stringstream message_ss;
		unordered_set<Card> init_unselected_cards = m_unselected_cards, init_selected_cards = m_selected_cards;

		for (int j = 0; j < test_cases; j++)
		{
			emit GameEquityCalculated(j);

			m_unselected_cards = init_unselected_cards;
			m_selected_cards = init_selected_cards;
			m_board.ClearBoard(m_flop_set, m_turn_set, m_river_set);
			GenerateCardsForPlayers();

			GenerateBoard();

			calculated_winners.clear();
			players_with_handvalue = EvaluatePlayers(calculated_winners);
			for (const auto& winner : calculated_winners)
				player_wins[winner]++;
		}

		m_board.ClearBoard(m_flop_set, m_turn_set, m_river_set);
		m_unselected_cards = init_unselected_cards;
		m_selected_cards = init_selected_cards;

		for (size_t i = 0; i < m_known_players.size(); i++)
			m_known_players[i].SetEquity(player_wins[m_known_players[i].GetID()] / test_cases * 100);
	}

	const std::vector<Player>& EquityCalculator::GetKnownPlayers() const
	{
		return m_known_players;
	}

	std::ostream& operator<<(std::ostream& out, const Strategy& strategy)
	{
		switch (strategy)
		{
			case Raise:out << "Raise"; break;
			case ReRaise:out << "ReRaise"; break;
			case Call:out << "Call"; break;
			case Call20:out << "Call20"; break;
			case Check:out << "Check"; break;
			case Fold:out << "Fold"; break;
			case SlowPlay:out << "SlowPlay"; break;
			case CheckFold:out << "CheckFold"; break;
			case RaiseFold:out << "RaiseFold"; break;
			case RaiseCall:out << "RaiseCall"; break;
			case UndefinedStrategy:out << "UndefinedStrategy"; break;
		}

		return out;
	}
}