#pragma once

#include <QWidget>
#include "ui_PlayerView.h"
#include <unordered_set>
#include "Constants.h"
#include "Card.h"

namespace poker
{
	class PlayerView : public QWidget
	{
		Q_OBJECT

	public:
		PlayerView(QWidget *parent = Q_NULLPTR);
		std::vector<Card> SelectPlayerCards(const std::unordered_set<Card>& selected_cards);
		void SetPlayerType(PlayerType& player_type);

		QPushButton* GetSelectCardButton() const;
		QPushButton* GetTypeButton() const;
		QCheckBox* GetPlayerCheckBox() const;
		QLabel* GetPlayerPositionLabel() const;
		QRadioButton* GetPlayerRadioButton() const;
		QLineEdit* GetPlayerLineEdit() const;

		void RemovePlayerViewCards();
	private:
		void SetPlayerCards(const std::vector<Card>& player_cards);

		Ui::PlayerView ui;
		std::unordered_set<Card> m_selected_cards;
	};
}