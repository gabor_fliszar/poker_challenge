#include "CardsWindow.h"
#include <QPushButton>
#include <algorithm>
#include <vector>
#include "Constants.h"

using std::unordered_set;
using std::vector;
using std::find;

namespace poker
{
	CardsWindow::CardsWindow(const std::unordered_set<Card>& selected_cards, std::vector<Card>* new_cards, QWidget *parent, CardsType cards_type)
		: QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint), m_new_cards(new_cards)
	{
		ui.setupUi(this);

		this->setFixedSize(CardsWindow_width, CardsWindow_height);
		for (int i = 0; i < kPlayingCardsCount; i++)
		{
			Card card(i);
			QString card_name = card.GetDeckName().c_str();
			QPushButton* cardButton = new QPushButton(this);

			cardButton->setFixedSize(kPlayingCardWidth, kPlayingCardHeight);

			cardButton->move((i % kPlayingCardTypeCount) * (kPlayingCardWidth + kCardDistance), i / kPlayingCardTypeCount * (kPlayingCardHeight + kCardDistance));
			cardButton->setIcon(QIcon(":/pngs/cards_png/" + card_name + ".png"));
			cardButton->setIconSize(QSize(kPlayingCardWidth - 4, kPlayingCardHeight - 4));
			cardButton->setDisabled(selected_cards.find(i) != selected_cards.end() ? true : false);

			connect(cardButton, &QPushButton::clicked, [=]() {SetPlayerCards(cardButton, i); });
		}
		m_ok_button = new QPushButton(this);
		m_ok_button->move(820, 440);
		m_ok_button->setFixedSize(60, 25);
		m_ok_button->setText("OK");
		m_ok_button->setDisabled(true);

		m_cancel_button = new QPushButton(this);
		m_cancel_button->move(889, 440);
		m_cancel_button->setFixedSize(60, 25);
		m_cancel_button->setText("Cancel");

		connect(m_cancel_button, &QPushButton::clicked, [this]() {this->close(); });
		connect(m_ok_button, &QPushButton::clicked, [this]() {ApplySelectedCards(); });

		if (cards_type == PlayerCards)
			m_maxCards = 2;
		else if (cards_type == FlopCards)
			m_maxCards = 3;
		else
			m_maxCards = 1;
	}

	void CardsWindow::SetPlayerCards(QPushButton * btn, int i)
	{
		Card card(i);

		if (m_player_cards.find(card) == m_player_cards.end() && m_player_cards.size() < m_maxCards)
		{
			btn->setStyleSheet("border:5px solid #FF0000;");
			m_player_cards.insert(card);
		}
		else if (m_player_cards.find(card) != m_player_cards.end())
		{
			btn->setStyleSheet("");
			m_player_cards.erase(card);
		}
		m_ok_button->setDisabled(m_player_cards.size() == m_maxCards ? false : true);
	}

	void CardsWindow::ApplySelectedCards()
	{
		for (const auto& card : m_player_cards)
			m_new_cards->push_back(card);

		this->close();
	}
}