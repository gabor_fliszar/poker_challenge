#pragma once
#include "HandValue.h"
#include "Board.h"
#include "Player.h"
#include <bitset>

namespace poker
{

	class HandValueCalculator2
	{	
		uint64_t m_allcards;
		
		void CheckRoyalFlush(HandValue& hand_value);
		bool HasStraightFlush(HandValue& hand_value);
		bool HasStraight(HandValue& hand_value);
		bool HasFlush(HandValue& hand_value);
		bool HasFullHouse(HandValue& hand_value);
		bool HasPair(HandValue& hand_value);
		bool HasTwoPairs(HandValue& hand_value);
		bool HasThreeOfAKind(HandValue& hand_value);
		bool HasFourOfAKind(HandValue& hand_value);
		HandValue GetHighestCards();
		void InitAllCardsWithPlayerCards(const Player& player);

	public:
		HandValueCalculator2(const Board & board);
		HandValue CalculateHandValue(const Player & player);
	};
}

