#pragma once

#include <QDialog>
#include "ui_OpponentTypeWindow.h"
#include "Constants.h"

namespace poker
{
	class OpponentTypeWindow : public QDialog
	{
		Q_OBJECT

	public:
		OpponentTypeWindow(PlayerType& player_type, QWidget *parent = Q_NULLPTR);

		void ApplyOpponentType();

	private:
		Ui::OpponentTypeWindow ui;
		PlayerType& m_player_type;
	};
}