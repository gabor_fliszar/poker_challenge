#pragma once
#include <string>
#include <bitset>

namespace poker
{
	const int kCardDeckNameLength = 2;

	const int kCardTypePosition = 0;
	const int kCardSuitPosition = 1;

	const int kHeartsStartPosition = 0;
	const int kDiamondsStartPosition = 13;
	const int kClubsStartPosition = 26;
	const int kSpadesStartPosition = 39;

	const int kPositionShift = 2;

	const int kAcePosition = 12;
	const int kKingPosition = 11;
	const int kQueenPosition = 10;
	const int kJackPosition = 9;
	const int kTenPosition = 8;

	const int kPlayingCardTypeCount = 13;
	const int kPlayingCardSuitCount = 4;

	const int kPlayerCardsCount = 2;
	const int kFlopCount = 3;
	const int kTurnCount = 1;
	const int kRiverCount = 1;

	const int kPlayerMaxCount = 10;

	const int kFlopFirstEnd = 2;
	const int kFlopSecondEnd = 4;

	const int kPlayingCardsCount = 52;


	const double kPreflopHighestEquity = 22.0;
	const double kPreflopHighEquity = 17.0;
	const double kPreflopMiddleEquity = 15.0;
	const double kPreflopLowEquity = 12.0;

	const double kFlopCBetLimit = 20.0;
	const double kFlopStrongDrawLimit = 28.0;
	const double kFlopStrongHandLimit = 50.0;

	const double kTurnRaiseFoldUpperLimit = 25.0;
	const double kTurnRaiseFoldLowerLimit = 12.0;

	const int CardsWindow_width = 960;
	const int CardsWindow_height = 470;

	const int kPlayingCardWidth = 70;
	const int kPlayingCardHeight = 105;
	const int kCardDistance = 3;

	const int kMonteCarloDefaultTestCases = 10000;

	enum HandRank
	{
		UndefinedHandRank = -1,
		High_card,
		Pair,
		Two_pairs,
		Three_of_a_kind,
		Straight,
		Flush,
		Full_house,
		Four_of_a_kind,
		Straight_flush,
		Royal_flush
	};

	enum Strategy
	{
		UndefinedStrategy = -1,
		Raise,
		ReRaise,
		CheckFold,
		Call,
		Call20,
		Check,
		Fold,
		SlowPlay,
		RaiseFold,
		RaiseCall
	};

	enum Position
	{
		UndefinedPosition = -1,
		BB,
		SB,
		BU,
		CO,
		MP3,
		MP2,
		MP1,
		UTG2,
		UTG1,
		UTG
	};

	enum PlayerType
	{
		UndefinedType = -1,
		Rock,
		Nit,
		TAG,
		Maniac,
		LAG,
		CallingStation
	};

	enum  CardsType
	{
		PlayerCards, FlopCards, TurnCard, RiverCard
	};

	const std::string kReRaiseStr = "Raise and reraise! You have a strong hand. If someone raises after you, you should just keep on raising. ";
	const std::string kSlowPlayStr = "You hold a monster hand!"
		"Instead of playing as if you had a strong hand, you feign weakness and play the hand slowly and wait for later streets to bet or raise. "
		"Don't slowplay against too many opponents and don't slowplay on draw-heavy boards. Don't slowplay against passive opponents, either. "
		"You should only slowplay when you are certain that your opponent will take the bait and overplay his hand.";
	const std::string kCheckFoldStr = "Check, but if someone raises after you, you have to fold your hand.";
	const std::string kRaiseFoldStr = "Raise, but if someone raises after you, you have to fold your hand.";
	const std::string kRaiseCallStr = "Raise, but if only one player raises after you, you can call the bet."
		"If more player raise after you, you have to fold your hand.";
	const std::string kCallStr = "You can call the opponent bets but if someone raises after you, you have to fold your hand.";
	const std::string kCall20Str = "You have a small pair are speculating on hitting Three of a kind on the flop. "
		"You should only call a raise, when your opponent has at least 20x the raise amount in his stack. "
		"By the way, this applies to you as well. You must also have 20x the raise amount. "
		"You can only win as much money as you have in your stack, "
		"so if your opponent has 20x the raise amount but you don't, it really doesn't help you."
		"If someone raises after you, you have to fold your hand.";
	const std::string kFoldStr = "You have to fold your hand.";


	class Masks
	{
		const static std::bitset<kPlayingCardsCount> masks[kPlayingCardTypeCount] = 
		{}
	};
}