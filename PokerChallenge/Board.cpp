#include "Board.h"


using std::vector;
using std::out_of_range;

namespace poker
{
	Board::Board()
	{
		is_complete = false;
	}

	Board::Board(const vector<Card>& flop, const vector<Card>& turn, const vector<Card>& river) :
		m_flop(flop), m_turn(turn), m_river(river)
	{
		if (!turn.empty() && !river.empty())
			is_complete = true;
		else
			is_complete = false;
	}

	const vector<Card>& Board::GetFlop() const
	{
		return m_flop;
	}

	const vector<Card>& Board::GetTurn() const
	{
		return m_turn;
	}

	const vector<Card>& Board::GetRiver() const
	{
		return m_river;
	}

	void Board::SetFlop(const vector<Card>& flop)
	{
		m_flop = flop;
		Check();
		
	}

	void Board::SetTurn(const vector<Card>& turn)
	{
		m_turn = turn;
		Check();
	}

	void Board::SetRiver(const vector<Card>& river)
	{
		m_river = river;
		Check();
	}

	void Board::Check()
	{
		if (m_flop.size() == kFlopCount && m_turn.size() == kTurnCount && m_river.size() == kRiverCount)
			is_complete = true;
		else if (m_flop.size() > kFlopCount || m_turn.size() > kTurnCount || m_river.size() > kRiverCount)
			throw std::out_of_range("Board inconsistent");
	}

	void Board::ClearBoard(bool flop_set, bool turn_set, bool river_set)
	{
		is_complete = flop_set && turn_set && river_set;

		if (!flop_set)
			m_flop.clear();
		if (!turn_set)
			m_turn.clear();
		if (!river_set)
			m_river.clear();
	}

	bool Board::IsComplete() const
	{
		return is_complete;
	}
}