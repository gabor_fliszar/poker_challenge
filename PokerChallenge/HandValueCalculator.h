#pragma once

#include "HandValue.h"
#include "Board.h"
#include "Player.h"
#include <map>
#include <unordered_set>
#include <vector>

namespace poker
{
	//Class for calculating hand value
	class HandValueCalculator
	{
	public:
		HandValueCalculator(const Board & board);
		HandValue CalculateHandValue(const Player & player);
	private:
		//This function prepares the parameters and fills the m_allcards container
		void InitParameters(const Player & player);
		//Prepare the parameters and containers for CheckStraightAndFlush method
		void PreCheckPossibleStraightAndFlush();
		//This method checks whether the hand rank is flush or straight
		void CheckStraightAndFlush(HandValue& hand_value);
		//This method checks the repeating type ranks(pair, two pair, three-of-a-kind, etc) of a hand
		void CheckRepeatingTypeRanks(HandValue& hand_value);

		const Board& m_board;
		HandRank m_rank;
		int m_height;
		int m_kicker;
		int m_first_card_type;
		int m_second_card_type;
		int m_longest_straight;
		int m_highest_straight_card;
		bool m_staright_flush;

		
		std::map<int, std::unordered_set<int>> m_allcards;
		std::unordered_set<int> m_possible_staright_flush_types;
		std::vector<int> m_card_suits;
		std::map<int, int> m_card_types;
	};
}

