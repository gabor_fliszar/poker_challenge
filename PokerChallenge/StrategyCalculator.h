#pragma once
#include "Board.h"
#include "Player.h"
#include "Constants.h"
#include <unordered_set>

namespace poker
{
	//With this class can calculate a strategy for the hero player
	class StrategyCalculator
	{
	public:
		StrategyCalculator(const Board & board, std::unordered_set<PlayerType> opponents_types);
		void CalculateStrategy(Player& player, Strategy opponents_actions, int opponent_count) const;

	private:
		void CalculatePreflopStrategy(Player& player, Strategy opponents_actions) const;
		void CalculatePostflopStrategy(Player& player, Strategy opponents_actions, int opponent_count) const;
		bool HasType(PlayerType type) const;

		const std::unordered_set<PlayerType> m_opponents_types;
		const Board& m_board;
	};
}