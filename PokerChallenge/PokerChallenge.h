#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_PokerChallenge.h"
#include <unordered_set>
#include "Constants.h"
#include "Player.h"
#include "Board.h"
#include "HandValue.h"
#include "PlayerView.h"

namespace poker
{
	class PokerChallenge : public QMainWindow
	{
		Q_OBJECT

	public:
		PokerChallenge(QWidget *parent = Q_NULLPTR);
		void SetHero(int hero);
		void CalculateStrategyForHero();
		void SetPlayerEnabled(int player_id, bool enabled);

	public slots:
		void ShowAbout();
		void SetPlayers(int max);
		void SelectPlayerCards(int player);
		void SelectBoardCards(CardsType board_cards);
		void ClearCards();
		void ClearPlayerCards(int player);
		void SetMonteCarloTestCases();
		void SetPlayerType(int player);

	private:
		int CalculatePositions();
		void ClearBoard(CardsType board_cards);
		std::unordered_set<PlayerType> CollectOpponentTypes();
		Strategy GetOpponentsMove();
		std::string ConvertCardNumberToCardName(int card_number);
		const std::string& CreateExplanationOfCalculatedStrategy(Strategy strategy);

		Ui::PokerChallengeClass ui;

		std::unordered_set<Card> m_selected_cards;
		std::unordered_set<Card> m_unselected_cards;
		std::vector<Player> m_players;
		std::vector<PlayerView*> m_player_views;

		BoardView* m_board_view;
		Board m_board;

		int m_hero_id;
		int m_active_players;
		int m_monte_carlo_test_cases;
	};
	std::ostream& operator<<(std::ostream& out, const HandRank& value);
}