#pragma once

#include <QDialog>
#include "ui_SetupMonteCarloWindow.h"

namespace poker
{
	class SetupMonteCarloWindow : public QDialog
	{
		Q_OBJECT

	public:
		SetupMonteCarloWindow(int& test_cases, QWidget *parent = Q_NULLPTR);

	public slots:
		void SetTestCases(int test_cases);
		void ApplyTestCases();

	private:
		int& m_test_cases;
		Ui::SetupMonteCarloWindow ui;
	};
}