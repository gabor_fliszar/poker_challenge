#include "StrategyCalculator.h"
#include "HandValueCalculator.h"
#include <unordered_set>

using std::runtime_error;
using std::unordered_set;

namespace poker
{
	StrategyCalculator::StrategyCalculator(const Board & board, unordered_set<PlayerType> opponents_types) :
		m_board(board), m_opponents_types(std::move(opponents_types))
	{}

	void StrategyCalculator::CalculateStrategy(Player & player, Strategy opponents_actions, int opponent_count) const
	{
		if (m_board.GetFlop().empty() && m_board.GetTurn().empty() && m_board.GetRiver().empty())
			CalculatePreflopStrategy(player, opponents_actions);
		else
			CalculatePostflopStrategy(player, opponents_actions, opponent_count);
	}

	void StrategyCalculator::CalculatePreflopStrategy(Player & player, Strategy opponents_actions) const
	{
		if (player.GetEquity() > kPreflopHighestEquity) //22% < e AA, KK, QQ, AKs
			player.SetPreflopStrategy(ReRaise);
		else if (player.GetEquity() < kPreflopHighestEquity && player.GetEquity() > kPreflopHighEquity) //17% < e < 20% JJ, TT, AKo, AQs, AQo, AJs, ATs, KQs, KJs, KTs
		{
			if (opponents_actions == Raise || opponents_actions == ReRaise)
				player.SetPreflopStrategy(Call20);
			else
				player.SetPreflopStrategy(RaiseFold);
		}
		else if (player.GetEquity() < kPreflopHighEquity && player.GetEquity() > kPreflopMiddleEquity) //15% < e < 17% AJo, ATo, A9s..A3s, KQo, KJo, QJo
		{
			switch (opponents_actions)
			{
				case ReRaise:
				case Raise: player.SetPreflopStrategy(Fold); break;
				case Call:
				{
					switch (player.GetPosition())
					{
						case UTG:
						case UTG1:
						case UTG2:
						case MP1:
						case MP2:
						case MP3: player.SetPreflopStrategy(Fold); break;
						case CO:
						case BU:player.SetPreflopStrategy(RaiseFold); break;
						case SB:
						case BB: player.SetPreflopStrategy(Call); break;
						case UndefinedPosition: throw runtime_error("UndefinedPosition");
						default:throw runtime_error("UndefinedPosition");
					}
					break;
				}
				case CheckFold:
				case Fold:
				{
					switch (player.GetPosition())
					{
						case UTG:
						case UTG1:
						case UTG2:player.SetPreflopStrategy(Fold); break;
						case MP1:
						case MP2:
						case MP3:
						case CO:
						case BU:
						case SB:
						case BB: player.SetPreflopStrategy(RaiseFold); break;
						case UndefinedPosition: throw runtime_error("UndefinedPosition");
						default:throw runtime_error("UndefinedPosition");
					}
					break;
				}
				case UndefinedStrategy:throw runtime_error("UndefinedAction");
				default:throw runtime_error("UndefinedAction");
			}
		}
		else if (player.GetEquity() < kPreflopMiddleEquity && player.GetEquity() > kPreflopLowEquity) //15% < e < 12% 99,..,22, A2s, A9o..A2o, T9s..23s
		{
			switch (opponents_actions)
			{
				case ReRaise:
				case Raise: player.SetPreflopStrategy(Fold); break;
				case Call:
				{
					switch (player.GetPosition())
					{
						case UTG:
						case UTG1:
						case UTG2:player.SetPreflopStrategy(Fold); break;
						case MP1:
						case MP2:
						case MP3:
						case CO:
						case BU: 
						case SB:
						case BB: player.SetPreflopStrategy(Call); break;
						case UndefinedPosition: throw runtime_error("UndefinedPosition");
						default:throw runtime_error("UndefinedPosition");
					}
					break;
				}
				case CheckFold:
				case Fold:
				{
					switch (player.GetPosition())
					{
						case UTG:
						case UTG1:
						case UTG2:player.SetPreflopStrategy(Fold); break;
						case MP1:
						case MP2:
						case MP3:player.SetPreflopStrategy(Call); break;
						case CO:
						case BU:player.SetPreflopStrategy(RaiseFold); break;
						case SB:
						case BB: player.SetPreflopStrategy(Call); break;
						case UndefinedPosition: throw runtime_error("UndefinedPosition");
						default:throw runtime_error("UndefinedPosition");
					}
					break;
				}
				case UndefinedStrategy:throw runtime_error("UndefinedAction");
				default:throw runtime_error("UndefinedAction");
			}
		}
		else
			player.SetPreflopStrategy(Fold);
	}

	void StrategyCalculator::CalculatePostflopStrategy(Player & player, Strategy opponents_actions, int opponent_count) const
	{
		HandValueCalculator hv_calculator(m_board);
		HandValue hand_value = hv_calculator.CalculateHandValue(player);
		unordered_set<HandRank> complete_hands{ Two_pairs,Three_of_a_kind,Straight,Flush };
		unordered_set<HandRank> monster_hands{ Full_house,Four_of_a_kind,Straight_flush,Royal_flush };

		if (!m_board.GetFlop().empty() && m_board.GetTurn().empty() && m_board.GetRiver().empty()) //flop
		{
			if (player.GetPreflopStrategy() == ReRaise || player.GetPreflopStrategy() == RaiseFold) //hero has preflop initiative
			{
				if (opponents_actions == CheckFold)
				{
					if (monster_hands.find(hand_value.rank) != monster_hands.end())
					{
						if (HasType(Maniac) || HasType(LAG))
							player.SetFlopStrategy(SlowPlay);
						else
							player.SetFlopStrategy(ReRaise);
					}
					else if (complete_hands.find(hand_value.rank) != complete_hands.end())
						player.SetFlopStrategy(ReRaise);
					else if (player.GetEquity() > kFlopStrongDrawLimit)
						player.SetFlopStrategy(RaiseCall);
					else if ((player.GetEquity() > kFlopCBetLimit) && (HasType(Rock) || HasType(Nit) || HasType(TAG)))
						player.SetFlopStrategy(RaiseFold);
					else
						player.SetFlopStrategy(CheckFold);
				}
				else //opponent(s) raised
				{
					if (monster_hands.find(hand_value.rank) != monster_hands.end() || complete_hands.find(hand_value.rank) != complete_hands.end())
						player.SetFlopStrategy(ReRaise);
					else if ((opponent_count == 1 && player.GetEquity() > kFlopStrongDrawLimit) ||
						(opponent_count > 1 && player.GetEquity() > kFlopStrongHandLimit))
						player.SetFlopStrategy(RaiseFold);
					else
						player.SetFlopStrategy(CheckFold);
				}
			}
			else // Opponent has preflop initiative
			{
				unordered_set<HandRank> complete_hands_opp_aggr{ Three_of_a_kind,Straight,Flush };

				if (monster_hands.find(hand_value.rank) != monster_hands.end())
					player.SetFlopStrategy(SlowPlay);
				else if (complete_hands_opp_aggr.find(hand_value.rank) != complete_hands_opp_aggr.end())
					player.SetFlopStrategy(ReRaise);
				else if (player.GetEquity() > kFlopStrongDrawLimit)
					player.SetFlopStrategy(Call);
				else
					player.SetFlopStrategy(CheckFold);
			}
		}
		else if (!m_board.GetFlop().empty() && !m_board.GetTurn().empty() && m_board.GetRiver().empty()) //turn
		{
			unordered_set<HandRank> turn_complete_hands{ Straight,Flush };
			unordered_set<HandRank> turn_middle_hands = { Two_pairs, Three_of_a_kind };

			if (monster_hands.find(hand_value.rank) != monster_hands.end())
			{
				if ((HasType(Maniac) || HasType(LAG)) && player.GetFlopStrategy() != SlowPlay)
					player.SetTurnStrategy(SlowPlay);
				else
					player.SetTurnStrategy(ReRaise);
			}
			else if (turn_complete_hands.find(hand_value.rank) != turn_complete_hands.end())
				player.SetTurnStrategy(ReRaise);
			else if (turn_middle_hands.find(hand_value.rank) != turn_middle_hands.end())
			{
				if (player.GetEquity() > kTurnRaiseFoldUpperLimit || (opponent_count == 1 && player.GetEquity() > kTurnRaiseFoldLowerLimit))
					player.SetTurnStrategy(RaiseFold);
				else
					player.SetTurnStrategy(CheckFold);
			}
			else
				player.SetTurnStrategy(CheckFold);
		}
		else //river
		{
			if (monster_hands.find(hand_value.rank) != monster_hands.end() || complete_hands.find(hand_value.rank) != complete_hands.end())
				player.SetRiverStrategy(ReRaise);
			else
				player.SetRiverStrategy(CheckFold);
		}

		player.SetHandValue(hand_value);

	}

	bool StrategyCalculator::HasType(PlayerType type) const
	{
		return !m_opponents_types.empty() && m_opponents_types.find(type) != m_opponents_types.end();
	}
}