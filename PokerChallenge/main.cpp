#include "PokerChallenge.h"
#include <QtWidgets/QApplication>

using namespace poker;

int main(int argc, char *argv[])
{
	srand(time(NULL));
	QApplication a(argc, argv);
	PokerChallenge w;
	w.show();
	return a.exec();
}
