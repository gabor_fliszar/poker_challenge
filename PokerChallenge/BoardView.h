#pragma once

#include <QWidget>
#include "ui_BoardView.h"
#include "Constants.h"
#include "Card.h"
#include <unordered_set>
#include <vector>

namespace poker
{
	class BoardView : public QWidget
	{
		Q_OBJECT

	public:
		BoardView(QWidget *parent = Q_NULLPTR);
		std::vector<Card> SelectBoardCards(CardsType board_cards, std::unordered_set<Card>& selected_cards);

		QPushButton* GetSelectCardButton(CardsType cards_type) const;
		QPushButton* GetSelectFlopButton() const;
		QPushButton* GetSelectTurnButton() const;
		QPushButton* GetSelectRiverButton() const;

		void RemoveBoardCards(CardsType board_cards);

	private:
		void SetEnableControls(bool enabled, CardsType board_cards);
		void SetFlopCards(const std::vector<Card>& board_cards);
		void SetTurnCard(const std::vector<Card>& board_cards);
		void SetRiverCard(const std::vector<Card>& board_cards);

		Ui::BoardView ui;
		std::unordered_set<Card> m_selected_cards;
	};
}