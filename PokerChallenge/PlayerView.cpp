#include "PlayerView.h"
#include "Constants.h"
#include "CardsWindow.h"
#include "PokerChallenge.h"
#include "OpponentTypeWindow.h"
#include <vector>

using std::unordered_set;
using std::vector;

namespace poker
{
	PlayerView::PlayerView(QWidget *parent)
		: QWidget(parent)
	{
		ui.setupUi(this);
	}

	vector<Card> PlayerView::SelectPlayerCards(const unordered_set<Card>& selected_cards)
	{
		m_selected_cards = selected_cards;
		RemovePlayerViewCards();
		vector<Card> player_cards;

		CardsWindow* playerCardsWindow = new CardsWindow(selected_cards, &player_cards, this, PlayerCards);

		playerCardsWindow->setAttribute(Qt::WA_DeleteOnClose);
		playerCardsWindow->setWindowModality(Qt::WindowModal);
		playerCardsWindow->exec();
		SetPlayerCards(player_cards);

		return player_cards;
	}

	void PlayerView::SetPlayerType(PlayerType& player_type)
	{
		OpponentTypeWindow* type_window = new OpponentTypeWindow(player_type, this);
		type_window->setAttribute(Qt::WA_DeleteOnClose);
		type_window->setWindowModality(Qt::WindowModal);
		type_window->exec();
	}

	QPushButton * PlayerView::GetSelectCardButton() const
	{
		return ui.select_cards_button;
	}

	QPushButton * PlayerView::GetTypeButton() const
	{
		return ui.select_type_button;
	}

	QCheckBox * PlayerView::GetPlayerCheckBox() const
	{
		return ui.select_player_chackbox;
	}

	QLabel * PlayerView::GetPlayerPositionLabel() const
	{
		return ui.player_position_label;
	}

	QRadioButton * PlayerView::GetPlayerRadioButton() const
	{
		return ui.hero_radio_button;
	}

	QLineEdit * PlayerView::GetPlayerLineEdit() const
	{
		return ui.cards_line_edit;
	}

	void PlayerView::SetPlayerCards(const vector<Card>& player_cards)
	{
		if (player_cards.size() == kPlayerCardsCount)
		{
			QString first_card_name = player_cards[0].GetDeckName().c_str(),
				second_card_name = player_cards[1].GetDeckName().c_str();

			ui.player_first_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + first_card_name + ".png);");
			ui.player_second_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + second_card_name + ".png);");

			ui.cards_line_edit->setText(first_card_name + second_card_name);
		}
	}

	void PlayerView::RemovePlayerViewCards()
	{
		if (!ui.cards_line_edit->text().isEmpty())
		{
			Card first_card(ui.cards_line_edit->text().left(kCardDeckNameLength).toStdString()),
				second_card(ui.cards_line_edit->text().right(kCardDeckNameLength).toStdString());

			m_selected_cards.erase(first_card.GetPosition());
			m_selected_cards.erase(second_card.GetPosition());

			ui.player_first_card->setStyleSheet("");
			ui.player_second_card->setStyleSheet("");

			ui.cards_line_edit->setText("");
		}
	}
}