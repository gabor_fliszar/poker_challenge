/********************************************************************************
** Form generated from reading UI file 'PlayerView.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAYERVIEW_H
#define UI_PLAYERVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PlayerView
{
public:
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QGridLayout *gridLayout;
    QCheckBox *select_player_chackbox;
    QRadioButton *hero_radio_button;
    QLabel *player_position_label;
    QPushButton *select_type_button;
    QPushButton *select_cards_button;
    QLineEdit *cards_line_edit;
    QHBoxLayout *horizontalLayout;
    QLabel *player_first_card;
    QLabel *player_second_card;

    void setupUi(QWidget *PlayerView)
    {
        if (PlayerView->objectName().isEmpty())
            PlayerView->setObjectName(QStringLiteral("PlayerView"));
        PlayerView->resize(115, 136);
        PlayerView->setMinimumSize(QSize(110, 120));
        PlayerView->setMaximumSize(QSize(564654, 16777215));
        PlayerView->setContextMenuPolicy(Qt::NoContextMenu);
        PlayerView->setAutoFillBackground(true);
        PlayerView->setStyleSheet(QStringLiteral(""));
        verticalLayout = new QVBoxLayout(PlayerView);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        frame = new QFrame(PlayerView);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setStyleSheet(QStringLiteral("#frame {border:1px solid white;}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        select_player_chackbox = new QCheckBox(frame);
        select_player_chackbox->setObjectName(QStringLiteral("select_player_chackbox"));
        select_player_chackbox->setMinimumSize(QSize(16, 16));
        select_player_chackbox->setMaximumSize(QSize(16, 16));
        select_player_chackbox->setChecked(true);

        gridLayout->addWidget(select_player_chackbox, 0, 0, 1, 1);

        hero_radio_button = new QRadioButton(frame);
        hero_radio_button->setObjectName(QStringLiteral("hero_radio_button"));
        hero_radio_button->setMinimumSize(QSize(16, 16));
        hero_radio_button->setMaximumSize(QSize(16, 16));
        hero_radio_button->setLayoutDirection(Qt::RightToLeft);
        hero_radio_button->setAutoExclusive(false);

        gridLayout->addWidget(hero_radio_button, 0, 1, 1, 1);

        player_position_label = new QLabel(frame);
        player_position_label->setObjectName(QStringLiteral("player_position_label"));
        player_position_label->setMinimumSize(QSize(42, 16));
        player_position_label->setMaximumSize(QSize(42, 16));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        player_position_label->setFont(font);
        player_position_label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(player_position_label, 0, 2, 1, 1);

        select_type_button = new QPushButton(frame);
        select_type_button->setObjectName(QStringLiteral("select_type_button"));
        select_type_button->setEnabled(true);
        select_type_button->setMinimumSize(QSize(20, 20));
        select_type_button->setMaximumSize(QSize(20, 20));
        select_type_button->setStyleSheet(QStringLiteral(""));
        QIcon icon;
        icon.addFile(QStringLiteral("loupe.png"), QSize(), QIcon::Normal, QIcon::Off);
        select_type_button->setIcon(icon);
        select_type_button->setIconSize(QSize(20, 20));

        gridLayout->addWidget(select_type_button, 1, 0, 1, 1);

        select_cards_button = new QPushButton(frame);
        select_cards_button->setObjectName(QStringLiteral("select_cards_button"));
        select_cards_button->setEnabled(true);
        select_cards_button->setMinimumSize(QSize(20, 20));
        select_cards_button->setMaximumSize(QSize(20, 20));
        select_cards_button->setStyleSheet(QStringLiteral(""));
        QIcon icon1;
        icon1.addFile(QStringLiteral("selectCard.png"), QSize(), QIcon::Normal, QIcon::Off);
        select_cards_button->setIcon(icon1);
        select_cards_button->setIconSize(QSize(20, 20));

        gridLayout->addWidget(select_cards_button, 1, 1, 1, 1);

        cards_line_edit = new QLineEdit(frame);
        cards_line_edit->setObjectName(QStringLiteral("cards_line_edit"));
        cards_line_edit->setMinimumSize(QSize(42, 20));
        cards_line_edit->setMaximumSize(QSize(42, 20));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        cards_line_edit->setFont(font1);
        cards_line_edit->setStyleSheet(QStringLiteral("color: #FFFFFF"));
        cards_line_edit->setReadOnly(true);

        gridLayout->addWidget(cards_line_edit, 1, 2, 1, 1);


        verticalLayout->addWidget(frame);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        player_first_card = new QLabel(PlayerView);
        player_first_card->setObjectName(QStringLiteral("player_first_card"));
        player_first_card->setMinimumSize(QSize(50, 70));
        player_first_card->setMaximumSize(QSize(50, 70));
        player_first_card->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(player_first_card);

        player_second_card = new QLabel(PlayerView);
        player_second_card->setObjectName(QStringLiteral("player_second_card"));
        player_second_card->setMinimumSize(QSize(50, 70));
        player_second_card->setMaximumSize(QSize(50, 70));
        player_second_card->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(player_second_card);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(PlayerView);

        QMetaObject::connectSlotsByName(PlayerView);
    } // setupUi

    void retranslateUi(QWidget *PlayerView)
    {
        PlayerView->setWindowTitle(QString());
#ifndef QT_NO_TOOLTIP
        select_player_chackbox->setToolTip(QApplication::translate("PlayerView", "Set player active/inactive", nullptr));
#endif // QT_NO_TOOLTIP
        select_player_chackbox->setText(QString());
#ifndef QT_NO_TOOLTIP
        hero_radio_button->setToolTip(QApplication::translate("PlayerView", "Set hero player", nullptr));
#endif // QT_NO_TOOLTIP
        hero_radio_button->setText(QString());
#ifndef QT_NO_TOOLTIP
        player_position_label->setToolTip(QApplication::translate("PlayerView", "Player position", nullptr));
#endif // QT_NO_TOOLTIP
        player_position_label->setText(QApplication::translate("PlayerView", "UTG", nullptr));
#ifndef QT_NO_TOOLTIP
        select_type_button->setToolTip(QApplication::translate("PlayerView", "Set opponent type", nullptr));
#endif // QT_NO_TOOLTIP
        select_type_button->setText(QString());
#ifndef QT_NO_TOOLTIP
        select_cards_button->setToolTip(QApplication::translate("PlayerView", "Select player cards", nullptr));
#endif // QT_NO_TOOLTIP
        select_cards_button->setText(QString());
#ifndef QT_NO_TOOLTIP
        cards_line_edit->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        cards_line_edit->setText(QString());
        cards_line_edit->setPlaceholderText(QString());
#ifndef QT_NO_TOOLTIP
        player_first_card->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        player_first_card->setText(QString());
#ifndef QT_NO_TOOLTIP
        player_second_card->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        player_second_card->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class PlayerView: public Ui_PlayerView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAYERVIEW_H
