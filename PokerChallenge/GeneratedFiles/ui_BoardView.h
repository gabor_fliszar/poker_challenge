/********************************************************************************
** Form generated from reading UI file 'BoardView.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOARDVIEW_H
#define UI_BOARDVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BoardView
{
public:
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QLineEdit *turn_card_line;
    QLineEdit *river_card_line;
    QLineEdit *flop_cards_line;
    QPushButton *river_card_button;
    QLabel *Flop_Label;
    QLabel *Turn_Label;
    QLabel *River_Label;
    QLabel *river_card;
    QPushButton *flop_cards_button;
    QGridLayout *gridLayout_9;
    QLabel *flop_first_card;
    QLabel *flop_second_card;
    QLabel *flop_third_card;
    QPushButton *turn_card_button;
    QLabel *turn_card;

    void setupUi(QWidget *BoardView)
    {
        if (BoardView->objectName().isEmpty())
            BoardView->setObjectName(QStringLiteral("BoardView"));
        BoardView->resize(477, 106);
        BoardView->setMinimumSize(QSize(477, 106));
        BoardView->setMaximumSize(QSize(477, 106));
        layoutWidget = new QWidget(BoardView);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(2, 2, 471, 100));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(30, 0, 25, 0);
        turn_card_line = new QLineEdit(layoutWidget);
        turn_card_line->setObjectName(QStringLiteral("turn_card_line"));
        turn_card_line->setMinimumSize(QSize(30, 20));
        turn_card_line->setMaximumSize(QSize(30, 20));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        font.setKerning(false);
        turn_card_line->setFont(font);
        turn_card_line->setStyleSheet(QStringLiteral("color: #FFFFFF"));
        turn_card_line->setReadOnly(true);

        gridLayout->addWidget(turn_card_line, 0, 6, 1, 1);

        river_card_line = new QLineEdit(layoutWidget);
        river_card_line->setObjectName(QStringLiteral("river_card_line"));
        river_card_line->setMinimumSize(QSize(30, 20));
        river_card_line->setMaximumSize(QSize(30, 20));
        river_card_line->setFont(font);
        river_card_line->setStyleSheet(QStringLiteral("color: #FFFFFF"));
        river_card_line->setReadOnly(true);

        gridLayout->addWidget(river_card_line, 0, 8, 1, 1);

        flop_cards_line = new QLineEdit(layoutWidget);
        flop_cards_line->setObjectName(QStringLiteral("flop_cards_line"));
        flop_cards_line->setMinimumSize(QSize(50, 20));
        flop_cards_line->setMaximumSize(QSize(50, 20));
        flop_cards_line->setFont(font);
        flop_cards_line->setStyleSheet(QStringLiteral("color: #FFFFFF"));
        flop_cards_line->setReadOnly(true);

        gridLayout->addWidget(flop_cards_line, 0, 2, 1, 1);

        river_card_button = new QPushButton(layoutWidget);
        river_card_button->setObjectName(QStringLiteral("river_card_button"));
        river_card_button->setEnabled(true);
        river_card_button->setMinimumSize(QSize(20, 20));
        river_card_button->setMaximumSize(QSize(20, 20));
        river_card_button->setStyleSheet(QStringLiteral(""));
        QIcon icon;
        icon.addFile(QStringLiteral("selectCard.png"), QSize(), QIcon::Normal, QIcon::Off);
        river_card_button->setIcon(icon);

        gridLayout->addWidget(river_card_button, 0, 9, 1, 1);

        Flop_Label = new QLabel(layoutWidget);
        Flop_Label->setObjectName(QStringLiteral("Flop_Label"));
        Flop_Label->setMinimumSize(QSize(20, 20));
        Flop_Label->setMaximumSize(QSize(100, 20));
        QFont font1;
        font1.setPointSize(10);
        font1.setBold(true);
        font1.setWeight(75);
        Flop_Label->setFont(font1);
        Flop_Label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(Flop_Label, 0, 0, 1, 1);

        Turn_Label = new QLabel(layoutWidget);
        Turn_Label->setObjectName(QStringLiteral("Turn_Label"));
        Turn_Label->setMinimumSize(QSize(30, 20));
        Turn_Label->setMaximumSize(QSize(30, 20));
        Turn_Label->setFont(font1);

        gridLayout->addWidget(Turn_Label, 0, 3, 1, 1);

        River_Label = new QLabel(layoutWidget);
        River_Label->setObjectName(QStringLiteral("River_Label"));
        River_Label->setMinimumSize(QSize(35, 20));
        River_Label->setMaximumSize(QSize(35, 20));
        River_Label->setFont(font1);

        gridLayout->addWidget(River_Label, 0, 7, 1, 1);

        river_card = new QLabel(layoutWidget);
        river_card->setObjectName(QStringLiteral("river_card"));
        river_card->setMinimumSize(QSize(50, 70));
        river_card->setMaximumSize(QSize(50, 70));

        gridLayout->addWidget(river_card, 1, 7, 1, 1);

        flop_cards_button = new QPushButton(layoutWidget);
        flop_cards_button->setObjectName(QStringLiteral("flop_cards_button"));
        flop_cards_button->setEnabled(true);
        flop_cards_button->setMinimumSize(QSize(20, 20));
        flop_cards_button->setMaximumSize(QSize(20, 20));
        flop_cards_button->setStyleSheet(QStringLiteral(""));
        flop_cards_button->setIcon(icon);

        gridLayout->addWidget(flop_cards_button, 0, 1, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setSpacing(6);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        flop_first_card = new QLabel(layoutWidget);
        flop_first_card->setObjectName(QStringLiteral("flop_first_card"));
        flop_first_card->setMinimumSize(QSize(50, 70));
        flop_first_card->setMaximumSize(QSize(50, 70));

        gridLayout_9->addWidget(flop_first_card, 0, 0, 1, 1);

        flop_second_card = new QLabel(layoutWidget);
        flop_second_card->setObjectName(QStringLiteral("flop_second_card"));
        flop_second_card->setMinimumSize(QSize(50, 70));
        flop_second_card->setMaximumSize(QSize(50, 70));

        gridLayout_9->addWidget(flop_second_card, 0, 1, 1, 1);

        flop_third_card = new QLabel(layoutWidget);
        flop_third_card->setObjectName(QStringLiteral("flop_third_card"));
        flop_third_card->setMinimumSize(QSize(50, 70));
        flop_third_card->setMaximumSize(QSize(50, 70));

        gridLayout_9->addWidget(flop_third_card, 0, 2, 1, 1);


        gridLayout->addLayout(gridLayout_9, 1, 0, 1, 3);

        turn_card_button = new QPushButton(layoutWidget);
        turn_card_button->setObjectName(QStringLiteral("turn_card_button"));
        turn_card_button->setEnabled(true);
        turn_card_button->setMinimumSize(QSize(20, 20));
        turn_card_button->setMaximumSize(QSize(20, 20));
        turn_card_button->setStyleSheet(QStringLiteral(""));
        turn_card_button->setIcon(icon);

        gridLayout->addWidget(turn_card_button, 0, 5, 1, 1);

        turn_card = new QLabel(layoutWidget);
        turn_card->setObjectName(QStringLiteral("turn_card"));
        turn_card->setMinimumSize(QSize(50, 70));
        turn_card->setMaximumSize(QSize(50, 70));

        gridLayout->addWidget(turn_card, 1, 5, 1, 1);


        retranslateUi(BoardView);

        QMetaObject::connectSlotsByName(BoardView);
    } // setupUi

    void retranslateUi(QWidget *BoardView)
    {
        BoardView->setWindowTitle(QString());
#ifndef QT_NO_TOOLTIP
        river_card_button->setToolTip(QApplication::translate("BoardView", "Select river card", nullptr));
#endif // QT_NO_TOOLTIP
        river_card_button->setText(QString());
        Flop_Label->setText(QApplication::translate("BoardView", "Flop", nullptr));
        Turn_Label->setText(QApplication::translate("BoardView", "Turn", nullptr));
        River_Label->setText(QApplication::translate("BoardView", "River", nullptr));
        river_card->setText(QString());
#ifndef QT_NO_TOOLTIP
        flop_cards_button->setToolTip(QApplication::translate("BoardView", "Select flop cards", nullptr));
#endif // QT_NO_TOOLTIP
        flop_cards_button->setText(QString());
        flop_first_card->setText(QString());
        flop_second_card->setText(QString());
        flop_third_card->setText(QString());
#ifndef QT_NO_TOOLTIP
        turn_card_button->setToolTip(QApplication::translate("BoardView", "Select turn card", nullptr));
#endif // QT_NO_TOOLTIP
        turn_card_button->setText(QString());
        turn_card->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class BoardView: public Ui_BoardView {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOARDVIEW_H
