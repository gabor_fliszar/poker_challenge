/********************************************************************************
** Form generated from reading UI file 'CardsWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CARDSWINDOW_H
#define UI_CARDSWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CardsWindow
{
public:

    void setupUi(QWidget *CardsWindow)
    {
        if (CardsWindow->objectName().isEmpty())
            CardsWindow->setObjectName(QStringLiteral("CardsWindow"));
        CardsWindow->setWindowModality(Qt::WindowModal);
        CardsWindow->resize(400, 300);
        CardsWindow->setStyleSheet(QStringLiteral("background-color: green"));

        retranslateUi(CardsWindow);

        QMetaObject::connectSlotsByName(CardsWindow);
    } // setupUi

    void retranslateUi(QWidget *CardsWindow)
    {
        CardsWindow->setWindowTitle(QApplication::translate("CardsWindow", "Card Selection", nullptr));
    } // retranslateUi

};

namespace Ui {
    class CardsWindow: public Ui_CardsWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CARDSWINDOW_H
