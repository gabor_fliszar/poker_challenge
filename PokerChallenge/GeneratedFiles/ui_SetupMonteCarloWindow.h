/********************************************************************************
** Form generated from reading UI file 'SetupMonteCarloWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETUPMONTECARLOWINDOW_H
#define UI_SETUPMONTECARLOWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SetupMonteCarloWindow
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *test_cases_label;
    QSlider *test_cases_slider;
    QPushButton *ok_button;

    void setupUi(QWidget *SetupMonteCarloWindow)
    {
        if (SetupMonteCarloWindow->objectName().isEmpty())
            SetupMonteCarloWindow->setObjectName(QStringLiteral("SetupMonteCarloWindow"));
        SetupMonteCarloWindow->resize(313, 130);
        SetupMonteCarloWindow->setMinimumSize(QSize(313, 130));
        SetupMonteCarloWindow->setMaximumSize(QSize(313, 130));
        SetupMonteCarloWindow->setStyleSheet(QStringLiteral("background-color: green"));
        verticalLayoutWidget = new QWidget(SetupMonteCarloWindow);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 311, 91));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        test_cases_label = new QLabel(verticalLayoutWidget);
        test_cases_label->setObjectName(QStringLiteral("test_cases_label"));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        test_cases_label->setFont(font);
        test_cases_label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(test_cases_label);

        test_cases_slider = new QSlider(verticalLayoutWidget);
        test_cases_slider->setObjectName(QStringLiteral("test_cases_slider"));
        test_cases_slider->setMinimum(1000);
        test_cases_slider->setMaximum(20000);
        test_cases_slider->setValue(10000);
        test_cases_slider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(test_cases_slider);

        ok_button = new QPushButton(SetupMonteCarloWindow);
        ok_button->setObjectName(QStringLiteral("ok_button"));
        ok_button->setGeometry(QRect(220, 100, 81, 23));

        retranslateUi(SetupMonteCarloWindow);

        QMetaObject::connectSlotsByName(SetupMonteCarloWindow);
    } // setupUi

    void retranslateUi(QWidget *SetupMonteCarloWindow)
    {
        SetupMonteCarloWindow->setWindowTitle(QApplication::translate("SetupMonteCarloWindow", "Monte Carlo Algorithm Test Cases", nullptr));
        test_cases_label->setText(QApplication::translate("SetupMonteCarloWindow", "10000", nullptr));
        ok_button->setText(QApplication::translate("SetupMonteCarloWindow", "OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SetupMonteCarloWindow: public Ui_SetupMonteCarloWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETUPMONTECARLOWINDOW_H
