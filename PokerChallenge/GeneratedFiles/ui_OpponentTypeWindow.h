/********************************************************************************
** Form generated from reading UI file 'OpponentTypeWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPPONENTTYPEWINDOW_H
#define UI_OPPONENTTYPEWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpponentTypeWindow
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QRadioButton *rock_button;
    QRadioButton *nit_button;
    QRadioButton *tag_button;
    QRadioButton *lag_button;
    QRadioButton *maniac_button;
    QRadioButton *calling_station_button;
    QPushButton *ok_button;

    void setupUi(QWidget *OpponentTypeWindow)
    {
        if (OpponentTypeWindow->objectName().isEmpty())
            OpponentTypeWindow->setObjectName(QStringLiteral("OpponentTypeWindow"));
        OpponentTypeWindow->resize(170, 183);
        OpponentTypeWindow->setMinimumSize(QSize(170, 183));
        OpponentTypeWindow->setMaximumSize(QSize(170, 183));
        OpponentTypeWindow->setStyleSheet(QStringLiteral("background-color: green"));
        verticalLayoutWidget = new QWidget(OpponentTypeWindow);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 160, 141));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        rock_button = new QRadioButton(verticalLayoutWidget);
        rock_button->setObjectName(QStringLiteral("rock_button"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        rock_button->setFont(font);
        rock_button->setStyleSheet(QStringLiteral("color: white; "));

        verticalLayout->addWidget(rock_button);

        nit_button = new QRadioButton(verticalLayoutWidget);
        nit_button->setObjectName(QStringLiteral("nit_button"));
        nit_button->setFont(font);
        nit_button->setStyleSheet(QStringLiteral("color: white; "));

        verticalLayout->addWidget(nit_button);

        tag_button = new QRadioButton(verticalLayoutWidget);
        tag_button->setObjectName(QStringLiteral("tag_button"));
        tag_button->setFont(font);
        tag_button->setStyleSheet(QStringLiteral("color: white; "));
        tag_button->setChecked(true);

        verticalLayout->addWidget(tag_button);

        lag_button = new QRadioButton(verticalLayoutWidget);
        lag_button->setObjectName(QStringLiteral("lag_button"));
        lag_button->setFont(font);
        lag_button->setStyleSheet(QStringLiteral("color: white; "));

        verticalLayout->addWidget(lag_button);

        maniac_button = new QRadioButton(verticalLayoutWidget);
        maniac_button->setObjectName(QStringLiteral("maniac_button"));
        maniac_button->setFont(font);
        maniac_button->setStyleSheet(QStringLiteral("color: white; "));

        verticalLayout->addWidget(maniac_button);

        calling_station_button = new QRadioButton(verticalLayoutWidget);
        calling_station_button->setObjectName(QStringLiteral("calling_station_button"));
        calling_station_button->setMinimumSize(QSize(0, 0));
        calling_station_button->setMaximumSize(QSize(3000, 3000));
        calling_station_button->setFont(font);
        calling_station_button->setStyleSheet(QStringLiteral("color: white; "));

        verticalLayout->addWidget(calling_station_button);

        ok_button = new QPushButton(OpponentTypeWindow);
        ok_button->setObjectName(QStringLiteral("ok_button"));
        ok_button->setGeometry(QRect(80, 150, 75, 23));
        ok_button->setStyleSheet(QStringLiteral("color: white; "));

        retranslateUi(OpponentTypeWindow);

        QMetaObject::connectSlotsByName(OpponentTypeWindow);
    } // setupUi

    void retranslateUi(QWidget *OpponentTypeWindow)
    {
        OpponentTypeWindow->setWindowTitle(QApplication::translate("OpponentTypeWindow", "Select Opponent Type", nullptr));
        rock_button->setText(QApplication::translate("OpponentTypeWindow", "Rock", nullptr));
        nit_button->setText(QApplication::translate("OpponentTypeWindow", "Nit", nullptr));
        tag_button->setText(QApplication::translate("OpponentTypeWindow", "TAG", nullptr));
        lag_button->setText(QApplication::translate("OpponentTypeWindow", "LAG", nullptr));
        maniac_button->setText(QApplication::translate("OpponentTypeWindow", "Maniac", nullptr));
        calling_station_button->setText(QApplication::translate("OpponentTypeWindow", "Calling Station", nullptr));
        ok_button->setText(QApplication::translate("OpponentTypeWindow", "OK", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OpponentTypeWindow: public Ui_OpponentTypeWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPPONENTTYPEWINDOW_H
