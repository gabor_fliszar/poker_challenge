/********************************************************************************
** Form generated from reading UI file 'PokerChallenge.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POKERCHALLENGE_H
#define UI_POKERCHALLENGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>
#include "BoardView.h"
#include "PlayerView.h"

QT_BEGIN_NAMESPACE

class Ui_PokerChallengeClass
{
public:
    QAction *actionExit;
    QAction *actionAbout;
    QAction *action_about;
    QAction *action_test;
    QAction *action_set_monte_carlo_algorithm_test_cases;
    QWidget *centralWidget;
    QFrame *frame;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout_3;
    poker::PlayerView *player_3_view;
    poker::PlayerView *player_4_view;
    poker::PlayerView *player_1_view;
    poker::PlayerView *player_7_view;
    poker::PlayerView *player_6_view;
    poker::PlayerView *player_2_view;
    poker::PlayerView *player_8_view;
    poker::PlayerView *player_0_view;
    poker::PlayerView *player_5_view;
    poker::PlayerView *player_9_view;
    poker::BoardView *board_view;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout_2;
    QComboBox *opponents_bets_ComboBox;
    QPushButton *clearButton;
    QPushButton *calculate_startegy_push_button;
    QGridLayout *gridLayout;
    QLabel *playersLabel;
    QComboBox *numberOfPlayersComboBox;
    QLabel *opponents_bets_Label;
    QWidget *gridLayoutWidget_2;
    QGridLayout *gridLayout_4;
    QTextEdit *textEdit;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;
    QMenu *menuTools;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *PokerChallengeClass)
    {
        if (PokerChallengeClass->objectName().isEmpty())
            PokerChallengeClass->setObjectName(QStringLiteral("PokerChallengeClass"));
        PokerChallengeClass->resize(773, 619);
        PokerChallengeClass->setMinimumSize(QSize(773, 619));
        PokerChallengeClass->setMaximumSize(QSize(773, 619));
        PokerChallengeClass->setLayoutDirection(Qt::LeftToRight);
        PokerChallengeClass->setStyleSheet(QStringLiteral(""));
        actionExit = new QAction(PokerChallengeClass);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionAbout = new QAction(PokerChallengeClass);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        action_about = new QAction(PokerChallengeClass);
        action_about->setObjectName(QStringLiteral("action_about"));
        QIcon icon;
        QString iconThemeName = QStringLiteral("dialog-information	");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QStringLiteral("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_about->setIcon(icon);
        action_test = new QAction(PokerChallengeClass);
        action_test->setObjectName(QStringLiteral("action_test"));
        action_set_monte_carlo_algorithm_test_cases = new QAction(PokerChallengeClass);
        action_set_monte_carlo_algorithm_test_cases->setObjectName(QStringLiteral("action_set_monte_carlo_algorithm_test_cases"));
        centralWidget = new QWidget(PokerChallengeClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(0, 0, 771, 391));
        frame->setStyleSheet(QStringLiteral("background: url(:/backgroundImg/layout.png) center center fixed"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_5 = new QGridLayout(frame);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setContentsMargins(1, 1, 1, 1);
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        player_3_view = new poker::PlayerView(frame);
        player_3_view->setObjectName(QStringLiteral("player_3_view"));
        player_3_view->setMinimumSize(QSize(110, 120));
        player_3_view->setMaximumSize(QSize(110, 120));
        player_3_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_3_view, 2, 3, 1, 1);

        player_4_view = new poker::PlayerView(frame);
        player_4_view->setObjectName(QStringLiteral("player_4_view"));
        player_4_view->setMinimumSize(QSize(110, 120));
        player_4_view->setMaximumSize(QSize(110, 120));
        player_4_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_4_view, 2, 4, 1, 1);

        player_1_view = new poker::PlayerView(frame);
        player_1_view->setObjectName(QStringLiteral("player_1_view"));
        player_1_view->setMinimumSize(QSize(110, 120));
        player_1_view->setMaximumSize(QSize(110, 120));
        player_1_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_1_view, 2, 1, 1, 1);

        player_7_view = new poker::PlayerView(frame);
        player_7_view->setObjectName(QStringLiteral("player_7_view"));
        player_7_view->setMinimumSize(QSize(110, 120));
        player_7_view->setMaximumSize(QSize(110, 120));
        player_7_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_7_view, 0, 3, 1, 1);

        player_6_view = new poker::PlayerView(frame);
        player_6_view->setObjectName(QStringLiteral("player_6_view"));
        player_6_view->setMinimumSize(QSize(110, 120));
        player_6_view->setMaximumSize(QSize(110, 120));
        player_6_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_6_view, 0, 4, 1, 1);

        player_2_view = new poker::PlayerView(frame);
        player_2_view->setObjectName(QStringLiteral("player_2_view"));
        player_2_view->setMinimumSize(QSize(110, 120));
        player_2_view->setMaximumSize(QSize(110, 120));
        player_2_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_2_view, 2, 2, 1, 1);

        player_8_view = new poker::PlayerView(frame);
        player_8_view->setObjectName(QStringLiteral("player_8_view"));
        player_8_view->setMinimumSize(QSize(110, 120));
        player_8_view->setMaximumSize(QSize(110, 120));
        player_8_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_8_view, 0, 2, 1, 1);

        player_0_view = new poker::PlayerView(frame);
        player_0_view->setObjectName(QStringLiteral("player_0_view"));
        player_0_view->setMinimumSize(QSize(110, 120));
        player_0_view->setMaximumSize(QSize(110, 120));
        player_0_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_0_view, 1, 0, 1, 1);

        player_5_view = new poker::PlayerView(frame);
        player_5_view->setObjectName(QStringLiteral("player_5_view"));
        player_5_view->setMinimumSize(QSize(110, 120));
        player_5_view->setMaximumSize(QSize(110, 120));
        player_5_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_5_view, 1, 6, 1, 1);

        player_9_view = new poker::PlayerView(frame);
        player_9_view->setObjectName(QStringLiteral("player_9_view"));
        player_9_view->setMinimumSize(QSize(110, 120));
        player_9_view->setMaximumSize(QSize(110, 120));
        player_9_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(player_9_view, 0, 1, 1, 1);

        board_view = new poker::BoardView(frame);
        board_view->setObjectName(QStringLiteral("board_view"));
        board_view->setStyleSheet(QStringLiteral("background:transparent"));

        gridLayout_3->addWidget(board_view, 1, 1, 1, 4);


        gridLayout_5->addLayout(gridLayout_3, 0, 0, 1, 1);

        gridLayoutWidget = new QWidget(centralWidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(590, 390, 181, 181));
        gridLayout_2 = new QGridLayout(gridLayoutWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        opponents_bets_ComboBox = new QComboBox(gridLayoutWidget);
        opponents_bets_ComboBox->setObjectName(QStringLiteral("opponents_bets_ComboBox"));

        gridLayout_2->addWidget(opponents_bets_ComboBox, 2, 0, 1, 1);

        clearButton = new QPushButton(gridLayoutWidget);
        clearButton->setObjectName(QStringLiteral("clearButton"));

        gridLayout_2->addWidget(clearButton, 3, 0, 1, 1);

        calculate_startegy_push_button = new QPushButton(gridLayoutWidget);
        calculate_startegy_push_button->setObjectName(QStringLiteral("calculate_startegy_push_button"));

        gridLayout_2->addWidget(calculate_startegy_push_button, 4, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        playersLabel = new QLabel(gridLayoutWidget);
        playersLabel->setObjectName(QStringLiteral("playersLabel"));
        QFont font;
        font.setPointSize(10);
        font.setBold(true);
        font.setWeight(75);
        playersLabel->setFont(font);

        gridLayout->addWidget(playersLabel, 0, 0, 1, 1);

        numberOfPlayersComboBox = new QComboBox(gridLayoutWidget);
        numberOfPlayersComboBox->setObjectName(QStringLiteral("numberOfPlayersComboBox"));
        numberOfPlayersComboBox->setEditable(true);

        gridLayout->addWidget(numberOfPlayersComboBox, 1, 0, 1, 1);

        opponents_bets_Label = new QLabel(gridLayoutWidget);
        opponents_bets_Label->setObjectName(QStringLiteral("opponents_bets_Label"));
        opponents_bets_Label->setFont(font);
        opponents_bets_Label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(opponents_bets_Label, 2, 0, 1, 1, Qt::AlignLeft);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayoutWidget_2 = new QWidget(centralWidget);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(0, 390, 591, 181));
        gridLayout_4 = new QGridLayout(gridLayoutWidget_2);
        gridLayout_4->setSpacing(0);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        textEdit = new QTextEdit(gridLayoutWidget_2);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setReadOnly(true);

        gridLayout_4->addWidget(textEdit, 1, 0, 1, 1);

        progressBar = new QProgressBar(gridLayoutWidget_2);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(24);

        gridLayout_4->addWidget(progressBar, 0, 0, 1, 1);

        PokerChallengeClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(PokerChallengeClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 773, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QStringLiteral("menuTools"));
        PokerChallengeClass->setMenuBar(menuBar);
        statusBar = new QStatusBar(PokerChallengeClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        PokerChallengeClass->setStatusBar(statusBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionExit);
        menuHelp->addAction(action_about);
        menuTools->addAction(action_set_monte_carlo_algorithm_test_cases);

        retranslateUi(PokerChallengeClass);
        QObject::connect(actionExit, SIGNAL(triggered()), PokerChallengeClass, SLOT(close()));

        numberOfPlayersComboBox->setCurrentIndex(-1);


        QMetaObject::connectSlotsByName(PokerChallengeClass);
    } // setupUi

    void retranslateUi(QMainWindow *PokerChallengeClass)
    {
        PokerChallengeClass->setWindowTitle(QApplication::translate("PokerChallengeClass", "Poker Challenge", nullptr));
        actionExit->setText(QApplication::translate("PokerChallengeClass", "Quit", nullptr));
        actionAbout->setText(QApplication::translate("PokerChallengeClass", "About", nullptr));
        action_about->setText(QApplication::translate("PokerChallengeClass", "About", nullptr));
        action_test->setText(QApplication::translate("PokerChallengeClass", "Test with downloadad hand histories", nullptr));
        action_set_monte_carlo_algorithm_test_cases->setText(QApplication::translate("PokerChallengeClass", "Set Monte Carlo Algorithm Test Cases", nullptr));
        clearButton->setText(QApplication::translate("PokerChallengeClass", "Clear Selected Cards", nullptr));
        calculate_startegy_push_button->setText(QApplication::translate("PokerChallengeClass", "Calculate Strategy for Hero", nullptr));
        playersLabel->setText(QApplication::translate("PokerChallengeClass", "Players:", nullptr));
        numberOfPlayersComboBox->setCurrentText(QString());
        opponents_bets_Label->setText(QApplication::translate("PokerChallengeClass", "Opponents move:", nullptr));
        menuFile->setTitle(QApplication::translate("PokerChallengeClass", "File", nullptr));
        menuHelp->setTitle(QApplication::translate("PokerChallengeClass", "Help", nullptr));
        menuTools->setTitle(QApplication::translate("PokerChallengeClass", "Tools", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PokerChallengeClass: public Ui_PokerChallengeClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POKERCHALLENGE_H
