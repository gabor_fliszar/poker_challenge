/********************************************************************************
** Form generated from reading UI file 'AboutWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ABOUTWINDOW_H
#define UI_ABOUTWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutWindow
{
public:
    QPushButton *pushButton;
    QFrame *frame;
    QFrame *frame_2;
    QLabel *label;

    void setupUi(QWidget *AboutWindow)
    {
        if (AboutWindow->objectName().isEmpty())
            AboutWindow->setObjectName(QStringLiteral("AboutWindow"));
        AboutWindow->setWindowModality(Qt::WindowModal);
        AboutWindow->resize(362, 224);
        AboutWindow->setMinimumSize(QSize(362, 224));
        AboutWindow->setMaximumSize(QSize(362, 224));
        AboutWindow->setStyleSheet(QStringLiteral(""));
        pushButton = new QPushButton(AboutWindow);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(270, 190, 75, 23));
        frame = new QFrame(AboutWindow);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(0, 0, 361, 181));
        frame->setStyleSheet(QStringLiteral("background-color: rgb(255, 255, 255)"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame_2 = new QFrame(frame);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 10, 61, 51));
        frame_2->setStyleSheet(QStringLiteral("image: url(:/information/Information_icon.png)"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(100, 20, 221, 151));
        label->setMinimumSize(QSize(187, 0));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);
        label->setLayoutDirection(Qt::LeftToRight);
        label->setStyleSheet(QStringLiteral(""));

        retranslateUi(AboutWindow);
        QObject::connect(pushButton, SIGNAL(clicked()), AboutWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(AboutWindow);
    } // setupUi

    void retranslateUi(QWidget *AboutWindow)
    {
        AboutWindow->setWindowTitle(QApplication::translate("AboutWindow", "About Poker Challenge", nullptr));
        pushButton->setText(QApplication::translate("AboutWindow", "OK", nullptr));
        label->setText(QApplication::translate("AboutWindow", "<html><head/><body><p>Poker Challenge Version 1.0</p><p><br/></p><p>Copyright \302\251 2018 by Gabor Fliszar</p><p>e-mail: gabor.fliszar@gmail.com</p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AboutWindow: public Ui_AboutWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ABOUTWINDOW_H
