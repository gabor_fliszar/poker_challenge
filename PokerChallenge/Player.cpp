#include "Player.h"

namespace poker
{
	Player::Player(int id, const std::vector<Card>& player_cards, Position position) :
		m_id(id), m_cards(player_cards), m_position(position), m_equity(0.0), m_preflop_strategy(Strategy::UndefinedStrategy),
		m_flop_strategy(Strategy::UndefinedStrategy), m_turn_strategy(Strategy::UndefinedStrategy), m_river_strategy(Strategy::UndefinedStrategy),
		m_type(UndefinedType)
	{
	}


	const std::vector<Card>& Player::GetCards() const
	{
		return m_cards;
	}

	void Player::SetCards(const std::vector<Card>& player_cards)
	{
		m_cards = player_cards;
	}

	void Player::SetEquity(double equity)
	{
		m_equity = equity;
	}

	void Player::SetPosition(Position position)
	{
		m_position = position;
	}

	void Player::SetFlopStrategy(Strategy flop_startegy)
	{
		m_flop_strategy = flop_startegy;
	}

	void Player::SetTurnStrategy(Strategy turn_strategy)
	{
		m_turn_strategy = turn_strategy;
	}

	void Player::SetRiverStrategy(Strategy river_strategy)
	{
		m_river_strategy = river_strategy;
	}

	void Player::SetPreflopStrategy(Strategy preflop_strategy)
	{
		m_preflop_strategy = preflop_strategy;
	}

	Strategy Player::GetPreflopStrategy() const
	{
		return m_preflop_strategy;
	}

	Strategy Player::GetFlopStrategy() const
	{
		return m_flop_strategy;
	}

	Strategy Player::GetTurnStrategy() const
	{
		return m_turn_strategy;
	}

	Strategy Player::GetRiverStrategy() const
	{
		return m_river_strategy;
	}

	PlayerType Player::GetPlayerType() const
	{
		return m_type;
	}

	const HandValue& Player::GetHandValue() const
	{
		return m_hand_value;
	}

	void Player::SetHandValue(const HandValue& hand_value)
	{
		m_hand_value = hand_value;
	}

	void Player::SetType(PlayerType type)
	{
		m_type = type;
	}

	double Player::GetEquity() const
	{
		return m_equity;
	}

	Position Player::GetPosition() const
	{
		return m_position;
	}

	void Player::ClearPlayerCards()
	{
		m_cards.clear();
	}

	int Player::GetID() const
	{
		return m_id;
	}

	bool Player::operator==(const Player & rhs) const
	{
		return m_id == rhs.m_id &&
			m_cards == rhs.m_cards &&
			m_position == rhs.m_position &&
			m_equity == rhs.m_equity &&
			m_preflop_strategy == rhs.m_preflop_strategy &&
			m_flop_strategy == rhs.m_flop_strategy &&
			m_turn_strategy == rhs.m_turn_strategy &&
			m_river_strategy == rhs.m_river_strategy;
	}

	bool Player::operator!=(const Player & rhs) const
	{
		return !(*this == rhs);
	}
}