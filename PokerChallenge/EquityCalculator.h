#pragma once
#include <vector>
#include <string>
#include "HandValue.h"
#include "Player.h"
#include "Board.h"
#include "Card.h"
#include <unordered_set>
#include <QWidget>

namespace poker
{
	//This class is for calculating the equity of player hand.
	//It implements the Monte Carlo algorithm for equity approximation 
	class EquityCalculator : public QWidget
	{
		Q_OBJECT

		std::vector<Player> m_known_players;
		std::vector<Player> m_unknown_players;
		Board m_board;
		const int m_max_player;
		std::unordered_set<Card> m_selected_cards;
		std::unordered_set<Card> m_unselected_cards;

		bool m_flop_set;
		bool m_turn_set;
		bool m_river_set;

		void GenerateCardsForPlayers();
		void GenerateBoard();

		std::vector<Card> GenerateRandomCards(int count);

	signals:
		void GameEquityCalculated(int game);

	public:
		EquityCalculator(const std::vector<Player>& players, const Board & board, int max_player);

		std::vector<std::pair<Player, HandValue>> EvaluatePlayers(std::set<int>& winners) const;
		void SimulateEquityWithMonteCarlo(int test_cases);
		const std::vector<Player>& GetKnownPlayers() const;
	};

	std::ostream& operator<<(std::ostream& out, const Strategy& strategy);
}