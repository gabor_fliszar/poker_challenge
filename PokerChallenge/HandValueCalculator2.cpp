#include "HandValueCalculator2.h"

namespace poker
{

	HandValueCalculator2::HandValueCalculator2(const Board & board) 
	{
		//Init m_allcards with board cards
	}

	HandValue HandValueCalculator2::CalculateHandValue(const Player & player)
	{
		HandValue hand_value, straight;
		
		if (HasFlush( hand_value))
		{
			HandValue flush = hand_value;
			if (HasStraight( straight))
			{
				if (HasStraightFlush( hand_value))
				{
					CheckRoyalFlush( hand_value);
					return hand_value;
				}

				if (HasFourOfAKind( hand_value))
					return hand_value;

				if (HasFullHouse( hand_value))
					return hand_value;
			}
			return flush;
		}

		if (straight.rank == Straight)
			return straight;

		if (HasThreeOfAKind( hand_value))
			return hand_value;

		if (HasTwoPairs( hand_value))
			return hand_value;

		if (HasPair( hand_value))
			return hand_value;

		return GetHighestCards();
	}

	void HandValueCalculator2::CheckRoyalFlush(HandValue & hand_value)
	{
	}
	bool HandValueCalculator2::HasStraightFlush(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasStraight(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasFlush(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasFullHouse(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasPair(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasTwoPairs(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasThreeOfAKind(HandValue & hand_value)
	{
		return false;
	}
	bool HandValueCalculator2::HasFourOfAKind(HandValue & hand_value)
	{
		return false;
	}
	HandValue HandValueCalculator2::GetHighestCards()
	{
		HandValue high_card;
		high_card.rank = High_card;

		for (int i = kPlayingCardTypeCount-1; i >= 0; i--)
		{
			//for(int j=0;j< kPlayingCardSuitCount)
			//if(m_allcards[i] || m_allcards[i*kPlayingCardTypeCount])
		}


		return high_card;
	}
	void HandValueCalculator2::InitAllCardsWithPlayerCards(const Player & player)
	{
	}
}
