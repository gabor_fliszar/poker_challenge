#include "Card.h"
#include <string>

using std::runtime_error;
using std::to_string;

namespace poker
{

	Card::Card(const std::string& poker_card_name) : m_deck_name(poker_card_name)
	{
		if (poker_card_name.length() != kCardDeckNameLength)
			throw runtime_error("worng input length");

		char suit = poker_card_name[kCardSuitPosition];

		switch (suit)
		{
			case 'h': m_deck_position = kHeartsStartPosition;   break;
			case 'd': m_deck_position = kDiamondsStartPosition; break;
			case 'c': m_deck_position = kClubsStartPosition;    break;
			case 's': m_deck_position = kSpadesStartPosition;   break;
			default: throw runtime_error("worng suit");
		}

		char hand_type = poker_card_name[kCardTypePosition];

		if (isdigit(hand_type))
			m_deck_position += (hand_type - '0') - kPositionShift;
		else
		{
			char typeChar = poker_card_name[kCardTypePosition];
			switch (typeChar)
			{
				case 'A':m_deck_position += kAcePosition; break;
				case 'K':m_deck_position += kKingPosition; break;
				case 'Q':m_deck_position += kQueenPosition; break;
				case 'J':m_deck_position += kJackPosition; break;
				case 'T':m_deck_position += kTenPosition; break;
				default: throw runtime_error("worng type");
			}
		}
	}

	Card::Card(int poker_card_number) : m_deck_position(poker_card_number)
	{
		int type_position = poker_card_number % kPlayingCardTypeCount;

		switch (type_position)
		{
			case kTenPosition:	m_deck_name = "T"; break;
			case kJackPosition:	m_deck_name = "J"; break;
			case kQueenPosition:m_deck_name = "Q"; break;
			case kKingPosition:	m_deck_name = "K"; break;
			case kAcePosition:	m_deck_name = "A"; break;
			default: m_deck_name = to_string(type_position + kPositionShift); break;
		}

		switch (poker_card_number / kPlayingCardTypeCount)
		{
			case 0:m_deck_name += "h"; break;
			case 1:m_deck_name += "d"; break;
			case 2:m_deck_name += "c"; break;
			case 3:
			default:m_deck_name += "s"; break;
		}
	}



	bool Card::operator==(const Card & rhs) const
	{
		return m_deck_name == rhs.m_deck_name && m_deck_position == rhs.m_deck_position;
	}

	const std::string& Card::GetDeckName() const
	{
		return m_deck_name;
	}

	int Card::GetPosition() const
	{
		return m_deck_position;
	}
}