#include "AboutWindow.h"
#include <QPixmap>

namespace poker
{
	AboutWindow::AboutWindow(QWidget *parent)
		: QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint)
	{
		ui.setupUi(this);
		setFixedSize(362, 224);
	}
}