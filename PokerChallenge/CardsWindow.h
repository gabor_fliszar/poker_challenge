#pragma once

#include <QWidget>
#include <QDialog>
#include "ui_CardsWindow.h"
#include <unordered_set>
#include <vector>
#include "Card.h"

namespace poker
{
	class CardsWindow : public QDialog
	{
		Q_OBJECT

	public:

		CardsWindow(const std::unordered_set<Card>& selected_cards, std::vector<Card>* new_cards, QWidget *parent = Q_NULLPTR, CardsType cards_type = PlayerCards);

		void SetPlayerCards(QPushButton* btn, int card);
		void ApplySelectedCards();

	private:

		Ui::CardsWindow ui;
		QPushButton* m_ok_button, *m_cancel_button;
		std::vector<Card>* m_new_cards;
		std::unordered_set<Card> m_player_cards;
		size_t m_maxCards;
	};
}