#include "SetupMonteCarloWindow.h"

namespace poker
{
	SetupMonteCarloWindow::SetupMonteCarloWindow(int& test_cases, QWidget *parent)
		: QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint), m_test_cases(test_cases)
	{
		ui.setupUi(this);
		connect(ui.ok_button, SIGNAL(clicked()), this, SLOT(ApplyTestCases()));
		connect(ui.test_cases_slider, SIGNAL(valueChanged(int)), this, SLOT(SetTestCases(int)));
		SetTestCases(test_cases);
		ui.test_cases_slider->setValue(test_cases);
	}

	void SetupMonteCarloWindow::SetTestCases(int test_cases)
	{
		ui.test_cases_label->setText(QString::number(test_cases));
	}

	void SetupMonteCarloWindow::ApplyTestCases()
	{
		m_test_cases = ui.test_cases_slider->value();
		close();
	}
}