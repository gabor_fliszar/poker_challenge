#pragma once
#include "Constants.h"
#include <string>

namespace poker
{
	//Class for storing cards
	class Card
	{
		std::string m_deck_name;
		int m_deck_position;

	public:

		Card(const std::string& poker_card_name);
		Card(int poker_card_number);

		bool operator ==(const Card & rhs) const;

		const std::string& GetDeckName() const;
		int GetPosition() const;
	};
}

namespace std
{
	template<>
	struct hash<poker::Card>
	{
		size_t
			operator()(const poker::Card & obj) const
		{
			return hash<int>()(obj.GetPosition());
		}
	};
}