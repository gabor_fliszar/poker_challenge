#include "PokerChallenge.h"
#include "AboutWindow.h"
#include "CardsWindow.h"
#include "EquityCalculator.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QMessageBox>
#include "Card.h"
#include <string>
#include <sstream>
#include <math.h>
#include <stdexcept>
#include <cstdlib>
#include <QGraphicsPixmapItem>
#include <QGroupBox>
#include <iomanip> 
#include "OpponentTypeWindow.h"
#include "PlayerView.h"
#include <QHBoxLayout>
#include <unordered_set>
#include "StrategyCalculator.h"
#include "SetupMonteCarloWindow.h"

using std::vector;
using std::string;
using std::unordered_set;
using std::to_string;
using std::set;
using std::pair;
using std::stringstream;
using std::setprecision;
using std::endl;

namespace poker
{
	PokerChallenge::PokerChallenge(QWidget *parent)
		: QMainWindow(parent), m_player_views(kPlayerMaxCount), m_players(kPlayerMaxCount), m_monte_carlo_test_cases(kMonteCarloDefaultTestCases)
	{
		ui.setupUi(this);

		for (int i = 0; i < kPlayingCardsCount; i++) 
			m_unselected_cards.insert(Card(i));

		ui.opponents_bets_ComboBox->addItem("ReRaise");
		ui.opponents_bets_ComboBox->addItem("Raise");
		ui.opponents_bets_ComboBox->addItem("Call");
		ui.opponents_bets_ComboBox->addItem("CheckFold");
		ui.opponents_bets_ComboBox->setCurrentIndex(3);

		for (int i = 0; i < 10; i++)
		{
			PlayerView* player_view = findChild<PlayerView*>("player_" + QString::number(i) + "_view");

			m_player_views[i] = player_view;

			if (i > 0)
				ui.numberOfPlayersComboBox->addItem(to_string(i + 1).c_str());

			connect(player_view->GetSelectCardButton(), &QRadioButton::clicked, [=]() { SelectPlayerCards(i); });
			connect(player_view->GetPlayerCheckBox(), &QCheckBox::stateChanged, [=](bool state) { SetPlayerEnabled(i, state); });
			connect(player_view->GetPlayerRadioButton(), &QRadioButton::clicked, [=]() { SetHero(i); });
			connect(player_view->GetTypeButton(), &QRadioButton::clicked, [=]() { SetPlayerType(i); });

			m_players[i] = Player(i, vector<Card>(), static_cast<Position>(i));
			m_players[i].SetType(TAG);
		}

		m_hero_id = -1;
		m_active_players = kPlayerMaxCount;
		CalculatePositions();
		ui.numberOfPlayersComboBox->setCurrentText(to_string(kPlayerMaxCount).c_str());
		connect(ui.numberOfPlayersComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(SetPlayers(int)));
		connect(ui.action_about, SIGNAL(triggered()), this, SLOT(ShowAbout()));
		connect(ui.action_set_monte_carlo_algorithm_test_cases, SIGNAL(triggered()), this, SLOT(SetMonteCarloTestCases()));

		connect(ui.action_test, SIGNAL(triggered()), this, SLOT(TestWithDownloadedHH()));

		m_board_view = findChild<BoardView*>("board_view");

		connect(m_board_view->GetSelectFlopButton(), &QRadioButton::clicked, [this]() { SelectBoardCards(FlopCards); });
		connect(m_board_view->GetSelectTurnButton(), &QRadioButton::clicked, [this]() { SelectBoardCards(TurnCard); });
		connect(m_board_view->GetSelectRiverButton(), &QRadioButton::clicked, [this]() { SelectBoardCards(RiverCard); });

		ui.progressBar->hide();
		ui.textEdit->setAlignment(Qt::AlignLeft | Qt::AlignTop);

		connect(ui.clearButton, SIGNAL(clicked()), this, SLOT(ClearCards()));
		connect(ui.calculate_startegy_push_button, &QPushButton::clicked, [=]() { CalculateStrategyForHero(); });

	}

	void PokerChallenge::SetPlayers(int max)
	{
		for (int i = 0; i < kPlayerMaxCount; i++)
		{
			if (i > max + 1)
			{
				if (m_player_views[i]->GetPlayerCheckBox()->isChecked())
				{
					m_player_views[i]->GetPlayerCheckBox()->setChecked(false);
					SetPlayerEnabled(i, false);
				}
			}
			else
			{
				if (!m_player_views[i]->GetPlayerCheckBox()->isChecked())
				{
					m_player_views[i]->GetPlayerCheckBox()->setChecked(true);
					SetPlayerEnabled(i, true);
				}
			}
		}
	}

	void PokerChallenge::SetHero(int hero)
	{
		this->m_hero_id = hero;
		for (int i = 0; i < 10; i++)
		{
			if (i != hero)
			{
				QRadioButton* hero_button = m_player_views[i]->GetPlayerRadioButton();
				hero_button->setAutoExclusive(false);
				hero_button->setChecked(false);
				hero_button->setAutoExclusive(true);
				if(m_player_views[i]->GetPlayerCheckBox()->isChecked())
					m_player_views[i]->GetTypeButton()->setEnabled(true);
			}
			else
				m_player_views[i]->GetTypeButton()->setEnabled(false);
		}
	}

	void PokerChallenge::CalculateStrategyForHero()
	{
		if (m_hero_id != -1)
		{
			if (!m_players[m_hero_id].GetCards().empty())
			{
				stringstream message_ss;
				EquityCalculator eq(m_players, m_board, m_active_players);

				ui.progressBar->setRange(0, m_monte_carlo_test_cases - 1);
				ui.progressBar->show();
				connect(&eq, SIGNAL(GameEquityCalculated(int)), ui.progressBar, SLOT(setValue(int)));
				eq.SimulateEquityWithMonteCarlo(m_monte_carlo_test_cases);
				ui.progressBar->hide();

				for (const auto& player : eq.GetKnownPlayers())
						m_players[player.GetID()].SetEquity(player.GetEquity());

				for (const auto& player : m_players)
				{
					if (!player.GetCards().empty())
					{
						if (m_hero_id == player.GetID())
							message_ss << setprecision(4) << "Hero equity: " << player.GetEquity() << "%" << endl;
						else
							message_ss << setprecision(4) << "Opponent(s) " << m_player_views[player.GetID()]->GetPlayerPositionLabel()->text().toStdString() <<" equity: " << player.GetEquity() << "%" << endl;
					}
				}

				StrategyCalculator strategy_calculator(m_board,CollectOpponentTypes());
				strategy_calculator.CalculateStrategy(m_players[m_hero_id], GetOpponentsMove(), m_active_players - 1);

				if (!m_board.GetFlop().empty())
				{
					message_ss << "Hero is " + m_player_views[m_hero_id]->GetPlayerPositionLabel()->text().toStdString() + "\nRank:";
					message_ss << m_players[m_hero_id].GetHandValue().rank;
					if (!m_players[m_hero_id].GetHandValue().heights.empty())
					{
						message_ss << "\nHeight(s):";
						for(auto it = m_players[m_hero_id].GetHandValue().heights.rbegin(); it != m_players[m_hero_id].GetHandValue().heights.rend();++it)
							message_ss << (it != m_players[m_hero_id].GetHandValue().heights.rbegin() ? ", " : "") << ConvertCardNumberToCardName(*it);
					}
					if (!m_players[m_hero_id].GetHandValue().kickers.empty())
					{
						message_ss << "\nKicker(s):";
						for (auto it = m_players[m_hero_id].GetHandValue().kickers.rbegin(); it != m_players[m_hero_id].GetHandValue().kickers.rend(); ++it)
							message_ss << (it != m_players[m_hero_id].GetHandValue().kickers.rbegin() ? ", " : "") << ConvertCardNumberToCardName(*it);
					}
					message_ss << "\n";
				}
				if (m_board.GetFlop().empty() && m_board.GetTurn().empty() && m_board.GetRiver().empty())
				{
					message_ss << "Suggested preflop strategy: " << m_players[m_hero_id].GetPreflopStrategy() << endl;
					message_ss << "Explanation: " << CreateExplanationOfCalculatedStrategy(m_players[m_hero_id].GetPreflopStrategy()) << endl;
				}
				else if (!m_board.GetFlop().empty() && m_board.GetTurn().empty() && m_board.GetRiver().empty())
				{
					message_ss << "Suggested flop strategy: " << m_players[m_hero_id].GetFlopStrategy() << endl;
					message_ss << "Explanation: " << CreateExplanationOfCalculatedStrategy(m_players[m_hero_id].GetFlopStrategy()) << endl;
				}
				else if (!m_board.GetFlop().empty() && !m_board.GetTurn().empty() && m_board.GetRiver().empty())
				{
					message_ss << "Suggested turn strategy: " << m_players[m_hero_id].GetTurnStrategy() << endl;
					message_ss << "Explanation: " << CreateExplanationOfCalculatedStrategy(m_players[m_hero_id].GetTurnStrategy()) << endl;
				}
				else
				{
					message_ss << "Suggested river strategy: " << m_players[m_hero_id].GetRiverStrategy() << endl;
					message_ss << "Explanation: " << CreateExplanationOfCalculatedStrategy(m_players[m_hero_id].GetRiverStrategy()) << endl;
				}

				ui.textEdit->setText(message_ss.str().c_str());
			}
			else
			{
				QPalette sample_palette;
				sample_palette.setColor(QPalette::Window, Qt::white);
				sample_palette.setColor(QPalette::WindowText, Qt::red);

				QFont font = ui.textEdit->font();
				font.setPointSize(8);
				font.setBold(true);
				ui.textEdit->setFont(font);

				ui.textEdit->setAutoFillBackground(true);
				ui.textEdit->setPalette(sample_palette);
				ui.textEdit->setText("Please select cards for hero!");
			}
		}
		else
		{
			QPalette sample_palette = ui.textEdit->palette();
			sample_palette.setColor(QPalette::Window, Qt::white);
			sample_palette.setColor(QPalette::WindowText, Qt::red);

			QFont font = ui.textEdit->font();
			font.setPointSize(8);
			font.setBold(true);
			ui.textEdit->setFont(font);

			ui.textEdit->setAutoFillBackground(true);
			ui.textEdit->setPalette(sample_palette);
			ui.textEdit->setText("Please select a hero player!");
		}
	}

	std::ostream& operator<<(std::ostream& out, const Position& position)
	{
		switch (position)
		{
			case BB:out << "BB"; break;
			case SB:out << "SB"; break;
			case BU:out << "BU"; break;
			case CO:out << "CO"; break;
			case MP3:out << "MP3"; break;
			case MP2:out << "MP2"; break;
			case MP1:out << "MP1"; break;
			case UTG2:out << "UTG+2"; break;
			case UTG1:out << "UTG+1"; break;
			case UTG:out << "UTG"; break;
		}

		return out;
	}

	int PokerChallenge::CalculatePositions()
	{
		int active_count = 0;
		for (size_t i = 0; i < m_player_views.size(); i++)
		{
			if (m_player_views[i]->GetPlayerCheckBox()->isChecked())
			{
				stringstream position_text;
				Position position = static_cast<Position>(active_count);
				position_text << position;
				m_player_views[i]->GetPlayerPositionLabel()->setText(position_text.str().c_str());
				m_players[i].SetPosition(position);
				active_count++;
			}
			else
			{
				m_player_views[i]->GetPlayerPositionLabel()->setText("");
				m_players[i].SetPosition(UndefinedPosition);
			}
		}
		return active_count;
	}

	void PokerChallenge::ClearBoard(CardsType board_cards)
	{
		switch (board_cards)
		{
			case FlopCards:
			{
				for (const auto& card : m_board.GetFlop())
				{
					m_selected_cards.erase(card);
					m_unselected_cards.insert(card);
				}
				break;
			}
			case TurnCard:
			{
				for (const auto& card : m_board.GetTurn())
				{
					m_selected_cards.erase(card);
					m_unselected_cards.insert(card);
				}
				break;
			}
			case RiverCard:
			default:
			{
				for (const auto& card : m_board.GetRiver())
				{
					m_selected_cards.erase(card);
					m_unselected_cards.insert(card);
				}
				break;
			}
		}
	}

	unordered_set<PlayerType> PokerChallenge::CollectOpponentTypes()
	{
		unordered_set<PlayerType> opponent_types;
		for (int i = 0; i < kPlayerMaxCount; i++)
		{
			PlayerType type = m_players[i].GetPlayerType();

			if (m_players[i].GetID() != m_hero_id &&
				m_player_views[i]->GetPlayerCheckBox()->isChecked() &&
				type != UndefinedType)
				opponent_types.insert(type);
		}
		return opponent_types;
	}

	Strategy PokerChallenge::GetOpponentsMove()
	{
		if (ui.opponents_bets_ComboBox->currentText() == "ReRaise")
			return ReRaise;
		else if (ui.opponents_bets_ComboBox->currentText() == "Raise")
			return Raise;
		else if (ui.opponents_bets_ComboBox->currentText() == "Call")
			return Call;
		else
			return CheckFold;
	}

	string PokerChallenge::ConvertCardNumberToCardName(int card_number)
	{
		if (card_number < 8)
			return to_string(card_number + 2);
		else
			switch (card_number)
			{
				case 8:  return "Ten";
				case 9:  return "Jack";
				case 10: return "Queen";
				case 11: return "King";
				case 12:
				default: return "Ace";
			}
	}

	const string& PokerChallenge::CreateExplanationOfCalculatedStrategy(Strategy strategy)
	{
		switch (strategy)
		{
			case ReRaise:	return kReRaiseStr;
			case CheckFold:	return kCheckFoldStr;
			case Call:		return kCallStr;
			case Call20:	return kCall20Str;
			case SlowPlay:	return kSlowPlayStr;
			case RaiseFold:	return kRaiseFoldStr;
			case RaiseCall: return kRaiseCallStr;
			case Fold:
			default:		return kFoldStr;
		}
	}

	void PokerChallenge::SetPlayerEnabled(int player_id, bool enabled)
	{
		if (m_active_players < 3 && !enabled)
		{
			m_player_views[player_id]->GetPlayerCheckBox()->setChecked(true);
			ui.textEdit->setText("The minimum number of players is 2!");
		}
		else
		{
			m_player_views[player_id]->GetPlayerPositionLabel()->setEnabled(enabled);
			m_player_views[player_id]->GetPlayerRadioButton()->setEnabled(enabled);
			m_player_views[player_id]->GetPlayerRadioButton()->setVisible(enabled);
			m_player_views[player_id]->GetSelectCardButton()->setEnabled(enabled);
			m_player_views[player_id]->GetPlayerLineEdit()->setVisible(enabled);

			if ((enabled && !m_player_views[player_id]->GetPlayerRadioButton()->isChecked()) || !enabled)
				m_player_views[player_id]->GetTypeButton()->setEnabled(enabled);


			if (!enabled)
				ClearPlayerCards(player_id);

			m_active_players = CalculatePositions();
		}
		ui.numberOfPlayersComboBox->setCurrentText(to_string(m_active_players).c_str());
	}

	void PokerChallenge::SetPlayerType(int player)
	{
		PlayerType player_type = m_players[player].GetPlayerType();
		m_player_views[player]->SetPlayerType(player_type);
		m_players[player].SetType(player_type);
	}

	void PokerChallenge::SetMonteCarloTestCases()
	{
		SetupMonteCarloWindow* setup_montecarlo = new SetupMonteCarloWindow(m_monte_carlo_test_cases, this);
		setup_montecarlo->setAttribute(Qt::WA_DeleteOnClose);
		setup_montecarlo->setWindowModality(Qt::WindowModal);
		setup_montecarlo->exec();
	}

	void PokerChallenge::ClearCards()
	{
		for (const auto& player_view : m_player_views)
			player_view->RemovePlayerViewCards();

		m_board_view->RemoveBoardCards(FlopCards);
		m_board_view->RemoveBoardCards(TurnCard);
		m_board_view->RemoveBoardCards(RiverCard);

		m_board.ClearBoard();

		m_selected_cards.clear();
		m_unselected_cards.clear();

		for (int i = 0; i < kPlayingCardsCount; i++)
			m_unselected_cards.insert(Card(i));

		ui.textEdit->clear();
	}

	void PokerChallenge::ClearPlayerCards(int player)
	{
		for (const auto& card : m_players[player].GetCards())
		{
			m_selected_cards.erase(card);
			m_unselected_cards.insert(card);
		}
		m_players[player].ClearPlayerCards();
		m_player_views[player]->RemovePlayerViewCards();
	}

	void PokerChallenge::SelectPlayerCards(int player)
	{
		for (const auto& card : m_players[player].GetCards())
		{
			m_selected_cards.erase(card);
			m_unselected_cards.insert(card);
		}

		vector<Card> player_cards = m_player_views[player]->SelectPlayerCards(m_selected_cards);

		for (const auto& card : player_cards)
		{
			m_selected_cards.insert(card);
			m_unselected_cards.erase(card);
		}

		m_players[player].SetCards(player_cards);
	}

	void PokerChallenge::SelectBoardCards(CardsType board_cards)
	{
		ClearBoard(board_cards);
		vector<Card> board = m_board_view->SelectBoardCards(board_cards, m_selected_cards);

		for (const auto& card : board)
		{
			m_selected_cards.insert(card);
			m_unselected_cards.erase(card);
		}

		switch (board_cards)
		{
			case FlopCards:
				m_board.SetFlop(board);
				break;
			case TurnCard:
				m_board.SetTurn(board);
				break;
			case RiverCard:
			default:
				m_board.SetRiver(board);
				break;
		}
	}

	void PokerChallenge::ShowAbout()
	{
		AboutWindow* aboutWindow = new AboutWindow(this);
		aboutWindow->setAttribute(Qt::WA_DeleteOnClose);
		aboutWindow->setWindowModality(Qt::WindowModal);
		aboutWindow->exec();
	}
}