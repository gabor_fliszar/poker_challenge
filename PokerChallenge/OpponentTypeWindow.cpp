#include "OpponentTypeWindow.h"

namespace poker
{
	OpponentTypeWindow::OpponentTypeWindow(PlayerType& player_type, QWidget *parent)
		: QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint), m_player_type(player_type)
	{
		ui.setupUi(this);

		connect(ui.ok_button, &QPushButton::clicked, [this]() {ApplyOpponentType(); });

		switch (player_type)
		{
			case Rock:			ui.rock_button->setChecked(true); break;
			case Nit:			ui.nit_button->setChecked(true); break;
			case TAG:			ui.tag_button->setChecked(true); break;
			case LAG:			ui.lag_button->setChecked(true); break;
			case Maniac:		ui.maniac_button->setChecked(true); break;
			case CallingStation: 
			default:			ui.calling_station_button->setChecked(true); break;
		}
	}

	void OpponentTypeWindow::ApplyOpponentType()
	{
		if (ui.rock_button->isChecked())
			m_player_type = Rock;
		else if (ui.nit_button->isChecked())
			m_player_type = Nit;
		else if (ui.tag_button->isChecked())
			m_player_type = TAG;
		else if (ui.lag_button->isChecked())
			m_player_type = LAG;
		else if (ui.maniac_button->isChecked())
			m_player_type = Maniac;
		else
			m_player_type = CallingStation;

		this->close();
	}
}