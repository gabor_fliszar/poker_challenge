#include "BoardView.h"
#include "Card.h"
#include <unordered_set>
#include <vector>
#include "CardsWindow.h"

using std::unordered_set;
using std::vector;

namespace poker
{
	BoardView::BoardView(QWidget *parent)
		: QWidget(parent)
	{
		ui.setupUi(this);
		SetEnableControls(false, TurnCard);
		SetEnableControls(false, RiverCard);
	}

	QPushButton * BoardView::GetSelectCardButton(CardsType cards_type) const
	{
		switch (cards_type)
		{
			case FlopCards:	return ui.flop_cards_button;
			case TurnCard:	return ui.turn_card_button;
			case RiverCard:
			default:		return ui.river_card_button;
		}
	}

	vector<Card> BoardView::SelectBoardCards(CardsType board_cards_type,  unordered_set<Card>& selected_cards)
	{
		m_selected_cards = selected_cards;
		RemoveBoardCards(board_cards_type);
		vector<Card> board_cards;

		CardsWindow* playerCardsWindow = new CardsWindow(selected_cards, &board_cards, this, board_cards_type);

		playerCardsWindow->setAttribute(Qt::WA_DeleteOnClose);
		playerCardsWindow->setWindowModality(Qt::WindowModal);
		playerCardsWindow->exec();

		switch (board_cards_type)
		{
			case FlopCards:
				if (!board_cards.empty())
				{
					SetFlopCards(board_cards);
					SetEnableControls(true, TurnCard);
				}
				else
				{
					RemoveBoardCards(TurnCard);
					RemoveBoardCards(RiverCard);
					SetEnableControls(false, TurnCard);					
					SetEnableControls(false, RiverCard);
				}
				break;
			case TurnCard:
				if (!board_cards.empty())
				{
					SetEnableControls(true, RiverCard);
					SetTurnCard(board_cards);
				}
				else
				{
					RemoveBoardCards(RiverCard);
					SetEnableControls(false, RiverCard);
				}
				break;
			case RiverCard:
			default:	SetRiverCard(board_cards); break;
		}

		selected_cards = m_selected_cards;
		return board_cards;
	}

	void BoardView::RemoveBoardCards(CardsType board_cards_type)
	{
		switch (board_cards_type)
		{
			case FlopCards:
				if (!ui.flop_cards_line->text().isEmpty())
				{
					Card first_card(ui.flop_cards_line->text().left(kCardDeckNameLength).toStdString()),
						second_card(ui.flop_cards_line->text().mid(kCardDeckNameLength, kCardDeckNameLength).toStdString()),
						third_card(ui.flop_cards_line->text().right(kCardDeckNameLength).toStdString());

					m_selected_cards.erase(first_card.GetPosition());
					m_selected_cards.erase(second_card.GetPosition());
					m_selected_cards.erase(third_card.GetPosition());

					ui.flop_first_card->setStyleSheet("");
					ui.flop_second_card->setStyleSheet("");
					ui.flop_third_card->setStyleSheet("");

					ui.flop_cards_line->setText("");
					break;
				}
			case TurnCard:
				if (!ui.turn_card_line->text().isEmpty())
				{
					Card turn_card(ui.turn_card_line->text().toStdString());

					m_selected_cards.erase(turn_card.GetPosition());

					ui.turn_card->setStyleSheet("");

					ui.turn_card_line->setText("");
					break;
				}
			case RiverCard:
			default:
				if (!ui.river_card_line->text().isEmpty())
				{
					Card river_card(ui.river_card_line->text().toStdString());

					m_selected_cards.erase(river_card.GetPosition());

					ui.river_card->setStyleSheet("");

					ui.river_card_line->setText("");
					break;
				}
		}
	}

	void BoardView::SetFlopCards(const vector<Card>& board_cards)
	{
		if (board_cards.size() == kFlopCount)
		{
			QString first_card_name = board_cards[0].GetDeckName().c_str(),
				second_card_name = board_cards[1].GetDeckName().c_str(),
				third_card_name = board_cards[2].GetDeckName().c_str();

			ui.flop_first_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + first_card_name + ".png);");
			ui.flop_second_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + second_card_name + ".png);");
			ui.flop_third_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + third_card_name + ".png);");

			ui.flop_cards_line->setText(first_card_name + second_card_name + third_card_name);
		}
	}

	void BoardView::SetTurnCard(const vector<Card>& board_cards)
	{
		if (board_cards.size() == kTurnCount)
		{
			QString card_name = board_cards[0].GetDeckName().c_str();

			ui.turn_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + card_name + ".png);");

			ui.turn_card_line->setText(card_name);
		}
	}

	void BoardView::SetRiverCard(const vector<Card>& board_cards)
	{
		if (board_cards.size() == kRiverCount)
		{
			QString card_name = board_cards[0].GetDeckName().c_str();

			ui.river_card->setStyleSheet("border-image:url(:/pngs/cards_png/" + card_name + ".png);");

			ui.river_card_line->setText(card_name);
		}
	}

	QPushButton * BoardView::GetSelectFlopButton() const
	{
		return ui.flop_cards_button;
	}

	QPushButton * BoardView::GetSelectTurnButton() const
	{
		return ui.turn_card_button;
	}

	QPushButton * BoardView::GetSelectRiverButton() const
	{
		return ui.river_card_button;
	}

	void BoardView::SetEnableControls(bool enabled, CardsType board_cards)
	{
		switch (board_cards)
		{
			case FlopCards:
				ui.flop_cards_button->setEnabled(enabled);
				ui.flop_cards_line->setEnabled(enabled);
				break;
			case TurnCard:
				ui.turn_card_button->setEnabled(enabled);
				ui.turn_card_line->setEnabled(enabled);
				break;
			case RiverCard:
			default:
				ui.river_card_button->setEnabled(enabled);
				ui.river_card_line->setEnabled(enabled);
				break;
		}
	}
}