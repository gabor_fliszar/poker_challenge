#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <vector>
using namespace std;

int main()
{ 
	ifstream handHistoryFile("raw_handhistory.txt");
	ofstream outputFile("handhistory.txt");
	string line;
	int count = 0;
	while (getline(handHistoryFile, line))
	{
		vector<string> player_cards;
		if (line == "*** SHOW DOWN ***")
		{

			int j = 0;
			while (getline(handHistoryFile, line) && line != "*** SUMMARY ***")
			{
				auto it = line.find("shows");
				if (it != string::npos)
				{ 
					j++;
					auto from = line.find("["), to = line.find("]"), from_round = line.find("("), to_round = line.find(")");
					string player_card = to_string(j) + "|" + line.substr(from, to - from + 1) + "|" + line.substr(from_round, to_round - from_round + 1);
					player_cards.push_back(player_card);
				}
			}

			if (j > 1 && line == "*** SUMMARY ***")
			{
				cout << ++count << endl;
				while (line != "")
				{
					getline(handHistoryFile, line);
					if (line.find("Board") != string::npos)
					{
						string board = line;
						while (getline(handHistoryFile, line) && line != "")
						{
							auto it = line.find("showed");
							bool winner_found = false;
							if (it != string::npos && !winner_found)
							{
								auto from = line.find("["), to = line.find("]"), hand = line.find("with"), sep = line.find(",");
								string won_lost;
								if (line.find("won") != string::npos)
								{
									string won_cards = line.substr(from, to - from + 1);
									for (auto &card : player_cards)
									{
										if (card.find(won_cards) != string::npos)
										{
											card += "|won";
											winner_found = true;
											break;
										}
									}
								}
							}
						}
						outputFile << board << endl;
						for (const auto& player_card : player_cards)
							outputFile << player_card << endl;
						outputFile << endl;
					}
				}
			}
		}
	}
	outputFile.close();
	return 0;
}